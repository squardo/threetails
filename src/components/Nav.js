import React from 'react';
// import { Link } from 'react-router-dom';
import Logo from '../images/TT-LOGO-PNG.png';
// import EmailModal from '../components/EmailModal';

import { Navbar } from 'rbx';
import '../styles/my_button.css';
import './nav.scss';

const Nav = () => {
	function Agreement() {
		return <Agreement />;
	}

	return (
		<div className="container">
			<div className="navStyle">
				<Navbar transparent>
					<Navbar.Brand>
						<Navbar.Item href="http://www.threetailspets.llc">
							<img src={Logo} className="logo" alt="logo" />
						</Navbar.Item>
						<Navbar.Burger />
					</Navbar.Brand>
					<Navbar.Menu>
						<Navbar.Segment align="start">
							<Navbar.Item href="http://www.threetailspets.llc">
								Home
							</Navbar.Item>
							<Navbar.Item href="http://www.threetailspets.llc">
								About
							</Navbar.Item>
							<Navbar.Item>
								<a href="https://form.jotform.com/201559273014046">
									Important Files
								</a>
							</Navbar.Item>
						</Navbar.Segment>
						<Navbar.Segment align="end">
							<Navbar.Item>
								<a href="https://form.jotform.com/201674557747163">
									Contact Me
								</a>
							</Navbar.Item>
						</Navbar.Segment>
					</Navbar.Menu>
				</Navbar>
			</div>
		</div>
	);
};

export default Nav;
