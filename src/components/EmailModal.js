import React from 'react';

import Modali, { useModali } from '../../node_modules/modali';

import { useForm } from 'react-hook-form';
import '../styles/Button.css';
import '../styles/formStyles.css';

import plane from '../images/icons8-paper-plane-32.png';

const EmailModal = () => {
	const [ exampleModal, toggleEmailModal ] = useModali();
	const { register, handleSubmit, errors } = useForm();

	const onSubmit = (ev) => {
		ev.preventDefault();
		const form = ev.target;
		const data = new FormData(form);
		const xhr = new XMLHttpRequest();
		xhr.open(form.method, form.action);
		xhr.setRequestHeader('Accept', 'application/json');
		xhr.onreadystatechange = () => {
			if (xhr.readyState !== XMLHttpRequest.DONE) return;
			if (xhr.status === 200) {
				form.reset();
				this.setState({ status: 'SUCCESS' });
			} else {
				this.setState({ status: 'ERROR' });
			}
		};
		xhr.send(data);
	};

	return (
		<div>
			<button
				onClick={toggleEmailModal}
				className="bttn-simple bttn-md bttn-primary">
				Email Me
			</button>
			<Modali.Modal {...exampleModal}>
				<div className="flex-form">
					<form
						onSubmit={handleSubmit(onSubmit)}
						action="https://formspree.io/mknqbqgv"
						method="POST"
						className="normform">
						<h2 className="title">Email Me</h2>
						<div className="flex-input">
							<div className="formInput">
								<label for="text-input">Name</label>
								<input
									name="firstname"
									className="input-font"
									ref={register}
									type="text"
									id="text-input"
									tabIndex="1"
									required
									placeholder="Text Input ..."
								/>
							</div>
							<div className="formInput">
								<label for="email-input">Email</label>
								<input
									name="lastname"
									className="input-font"
									ref={register({ required: true })}
									placeholder="example@google.com"
								/>
								{errors.lastname && 'Last name is required.'}
							</div>
						</div>
						<div className="flex-input">
							<div className="formInput">
								<label for="number-input">Phone Number</label>
								<input
									type="number"
									name="number"
									ref={register}
									id="number-input"
									required
									tabindex="4"
									className="input-font"
									placeholder="(555-555-5555)"
								/>
								<div className="requirements">
									Please use numerical values only.
								</div>
							</div>
						</div>
						<div className="text-area">
							<label for="textarea">Textarea:</label>
							<textarea
								ref={register}
								rows="8"
								name="textarea"
								className="input-font"
								id="textarea"
							/>
						</div>
						<button className="btn btn--m btn--blue btn--full" type="submit">
							<div className="flexbutton">
								<span>
									<img src={plane} alt="" className="plane" />
								</span>
								<p className="submitSize">SUBMIT</p>
							</div>
						</button>
					</form>
				</div>
			</Modali.Modal>
		</div>
	);
};

export default EmailModal;
