/* eslint-disable no-unused-vars */
import React from 'react';
import parse from 'html-react-parser';

const Agreement = () => {
	return parse(`
<!doctype html>

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:title" content="">
  <meta property="og:type" content="">
  <meta property="og:url" content="">
  <meta property="og:image" content="">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">

  <meta name="theme-color" content="#fafafa">


<script src="https://cdn.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
<script src="https://cdn.jotfor.ms/static/jotform.forms.js?3.3.18236" type="text/javascript"></script>
<script src="https://cdn.jotfor.ms/pdf-importer/static/js/form-patch.js?3.3.18236" type="text/javascript"></script>
<script type="text/javascript">
	JotForm.init(function(){
if (window.JotForm && JotForm.accessible) $('input_2').setAttribute('tabindex',0);
if (window.JotForm && JotForm.accessible) $('input_3').setAttribute('tabindex',0);
if (window.JotForm && JotForm.accessible) $('input_4').setAttribute('tabindex',0);
if (window.JotForm && JotForm.accessible) $('input_7').setAttribute('tabindex',0);
      setTimeout(function() {
          $('input_7').hint('555-555-5555');
       }, 20);
if (window.JotForm && JotForm.accessible) $('input_52').setAttribute('tabindex',0);

 JotForm.calendarMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
 JotForm.calendarDays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
 JotForm.calendarOther = {"today":"Today"};
 var languageOptions = document.querySelectorAll('#langList li'); 
 for(var langIndex = 0; langIndex < languageOptions.length; langIndex++) { 
   languageOptions[langIndex].on('click', function(e) { setTimeout(function(){ JotForm.setCalendar("10", false, {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},"future":true,"past":true,"custom":false,"ranges":false,"start":"","end":""}); }, 0); });
 } 
 JotForm.setCalendar("10", false, {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},"future":true,"past":true,"custom":false,"ranges":false,"start":"","end":""});
if (window.JotForm && JotForm.accessible) $('input_11').setAttribute('tabindex',0);
if (window.JotForm && JotForm.accessible) $('input_34').setAttribute('tabindex',0);
if (window.JotForm && JotForm.accessible) $('input_45').setAttribute('tabindex',0);

 JotForm.calendarMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
 JotForm.calendarDays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
 JotForm.calendarOther = {"today":"Today"};
 var languageOptions = document.querySelectorAll('#langList li'); 
 for(var langIndex = 0; langIndex < languageOptions.length; langIndex++) { 
   languageOptions[langIndex].on('click', function(e) { setTimeout(function(){ JotForm.setCalendar("47", false, {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},"future":true,"past":false,"custom":false,"ranges":false,"start":"","end":""}); }, 0); });
 } 
 JotForm.setCalendar("47", false, {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},"future":true,"past":false,"custom":false,"ranges":false,"start":"","end":""});
 JotForm.formatDate({date:(new Date()), dateField:$("id_"+47)});
if (window.JotForm && JotForm.accessible) $('input_51').setAttribute('tabindex',0);

 JotForm.calendarMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
 JotForm.calendarDays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
 JotForm.calendarOther = {"today":"Today"};
 var languageOptions = document.querySelectorAll('#langList li'); 
 for(var langIndex = 0; langIndex < languageOptions.length; langIndex++) { 
   languageOptions[langIndex].on('click', function(e) { setTimeout(function(){ JotForm.setCalendar("19", false, {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},"future":true,"past":false,"custom":false,"ranges":false,"start":"","end":""}); }, 0); });
 } 
 JotForm.setCalendar("19", false, {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},"future":true,"past":false,"custom":false,"ranges":false,"start":"","end":""});
 JotForm.formatDate({date:(new Date()), dateField:$("id_"+19)});
if (window.JotForm && JotForm.accessible) $('input_20').setAttribute('tabindex',0);
	JotForm.newDefaultTheme = false;
	JotForm.newPaymentUIForNewCreatedForms = true;
	JotForm.importedPDF = "https://www.jotform.com/uploads/guest_fd9cf9ee26dde9a1/form_files/pfc_5ed9b7d73a393_Rainbow_protocal-merged_.pdf?nc=1";
	JotForm.importedPDFSettings = {"isConnected":"Yes","enableThumbnail":"Yes"};
      JotForm.alterTexts(undefined);
	JotForm.clearFieldOnHide="disable";
	JotForm.submitError="jumpToFirstError";
    /*INIT-END*/
	});

   JotForm.prepareCalculationsOnTheFly([null,{"name":"threeTails","qid":"1","text":"Three Tails Rainbow Protocol","type":"control_head"},{"description":"","name":"petsName","qid":"2","subLabel":"","text":"Pet's Name:","type":"control_textbox"},{"description":"","name":"petGuardian","qid":"3","subLabel":"","text":"Pet Guardian:","type":"control_textbox"},{"description":"","name":"emergencyContact","qid":"4","subLabel":"","text":"Emergency Contact:","type":"control_textbox"},{"name":"ltpgtltspanStylecolor","qid":"5","text":"Three Tails Pet Sitting’s policy is to have on file a Rainbow Protocol for senior dogs (10or older) and\u002For dogs with medical\u002Fphysical conditions that could be life threatening.This is a precautionary measure to assist us in following your wishes in the unlikelyevent your pet passes away while in our care. You can provide us with an emergencycontact who will assume the responsibility to pick up your loved one and follow throughon your arrangements. Or you can give Three Tails Pet Sitting the responsibility ofmaking arrangements for your pet at your veterinarian’s office and we will arrangetransportation there and pick up ashes for you.","type":"control_text"},null,{"description":"","name":"number","qid":"7","subLabel":"","text":"Number:","type":"control_textbox"},null,null,{"description":"","name":"date","qid":"10","text":"Date:","type":"control_datetime"},{"description":"","name":"witness","qid":"11","subLabel":"","text":"Witness:","type":"control_textbox"},{"description":"","labelText":"","name":"image2","qid":"12","text":"image2","type":"control_image"},null,null,null,{"description":"","labelText":"","name":"image3","qid":"16","text":"image3","type":"control_image"},{"name":"ltpgtltspanStylecolor17","qid":"17","text":"8)WEAKENED IMMUNE SYSTEM: Owner understands special-needs dogs, young puppies, and senior dogs naturally have a higher risk of injury, stress-related illnesses, weakened immune system, or exacerbation of any pre-existing condition. As such, by using Three Tails, LLC for boarding the owner is waiving any claim for injury or illness experienced by owner’s dog while in Three Tails, LLC care. 9)Owner further agrees to indemnify, defend, and hold Three Tails, LLC harmless against any claims by persons or entities other than owner related to owner’s dog(s) participation.\nOwner has read this agreement and understands that it is an agreement to indemnify Three Tails, LLC and the other released parties and it is a waiver and release of liability and a promise not to sue or make a claim. Owner is aware that this agreement is a contract between Three Tails, LLC and owner, and that owner cannot participate in Three Tails, LLC services unless owner signs this agreement, which owner is doing of free will.","type":"control_text"},null,{"description":"","name":"date19","qid":"19","text":"Date","type":"control_datetime"},{"description":"","name":"nameOf","qid":"20","subLabel":"","text":"Name of Owner (please print)","type":"control_textbox"},{"name":"ltpgtltspanStylecolor21","qid":"21","text":"3931 Lillian Guest Circle, Valdosta, GA 31605 | (229)221-0144 | threetailspets@gmail.com","type":"control_text"},{"name":"submit","qid":"22","text":"Submit","type":"control_button"},{"description":"","name":"iWant23","qid":"23","text":"I want my emergency contact to be called and to pick up my pet if they pass before my return. (however, if your emergency contact is unreachable I understand that Three Tails Pet Sitting will transport my pet to my veterinarian’s office on my behalf and the expenses will added to my bill)","type":"control_radio"},null,{"description":"","name":"doYou25","qid":"25","text":"Do you want us to contact you in the unlikely event your dog passes away while in our care? If yes please give us a number to reach you.","type":"control_checkbox"},null,null,null,{"description":"","name":"iAuthorize","qid":"29","text":"I authorize Three Tails Pet Sitting to transport my pet to myveterinarian’s office for cremation and wish to have my ashes returned.","type":"control_checkbox"},{"description":"","name":"iAuthorize30","qid":"30","text":"I authorize Three Tails Pet Sitting to transport my pet to myveterinarian’s office for cremation and do NOT wish to have my ashes returned.","type":"control_checkbox"},{"name":"input31","qid":"31","text":"By signing below you understand Three Tails Pet Sitting Rainbow Protocol and thatthere are fees associated with this. You give Three Tails Pet Sitting permission to followyour directions as stated above.","type":"control_text"},null,{"name":"input32","qid":"33","text":"This agreement is entered into by and between Three Tails, LLC, and dog owner.","type":"control_text"},{"description":"","name":"dogsName","qid":"34","subLabel":"","text":"Dog(s) name","type":"control_textbox"},null,null,{"name":"input35","qid":"37","text":"\n\nI (owner) represent that I am the legal owner of the above named dog(s) and I assume all risks, dangers, and responsibility for injuries to the named dog(s). Owner understands and agrees that owner is solely responsible for any harm while owner's dog(s) is\u002Fare attending boarding.\n\n\nOwner is aware that owner(s) dog(s) will co-mingle with dogs owned by different owners in a supervised environment unless otherwise specified during the consultation.\n\n\nPHOTO AND VIDEO RELEASE: Three Tails, LLC loves to post pictures and videos on Facebook and\u002For Instagram. Owner agrees to allow Three Tails, LLC to use owner’s pet’s name and any images or likeness of owner’s pet taken while he\u002Fshe is at ThreeTails, LLC, in any form, for use at any time, in any media, marketing, advertising, illustration, trade or promotional materials without compensation, and owner releases to Three Tails, LLC all rights that owner may possess or claim to such image, likeness, recording, etc.\n\n\nDOG’S HEALTH: Owner further understands and agrees that owner’s dog(s) are healthy and will at times while attending Three Tails, LLC have current vaccinations for Rabies, Distemper, Parvo, and Bordetella. Owner further understands that even if owner’s dog(s)is vaccinated for Bordetella (Kennel Cough) there is a chance that the owner’s dog(s) can still contract Kennel Cough. Owner agrees that owner will not hold Three Tails, LLC responsible if owner’s dog(s) contracts Kennel Cough or other dog-dog transmitted ailments.\n\n\nThree Tails, LLC will make every effort to keep all dogs safe however owner recognizes that there are inherent risks of illness or injury when dealing with animals. Such risks include, but are not limited to problems resulting from rough play and Kennel Cough.\n\n\nRELEASE OF LIABILITY: Owner understands and agrees that during normal dog play, owner’s dog may sustain injuries. Dog play is monitored by Three Tails, LLC to best avoid injury but scratches, punctures, torn ligaments, and other injuries may occur despite the best supervision. Owner further understands and agrees that neither boarding nor Three Tails, LLC owner, Hannah White will be liable for any illness, injury,death, and\u002For escape of owner’s dog(s) provided that reasonable care and precautions are followed, and owner hereby releases Three Tails, LLC owner, Hannah White of any liability of any kind whatsoever arising from or as a result of owner’s dog(s) attending Three Tails, LLC. OWNER IS AWARE AND UNDERSTANDS THREE TAILS, LLC DOES NOT CARRY LIABILITY INSURANCE.\n\n\nWEAKENED IMMUNE SYSTEM: Owner understands special-needs dogs, young puppies, and senior dogs naturally have a higher risk of injury, stress-related illnesses, weakened immune system, or exacerbation of any pre-existing condition. As such, by using Three Tails, LLC for boarding the owner is waiving any claim for injury or illness experienced by owner’s dog while in Three Tails, LLC care.\n\n\nOwner further agrees to indemnify, defend, and hold Three Tails, LLC harmless against any claims by persons or entities other than owner related to owner’s dog(s) participation.\n\n","type":"control_text"},null,null,null,{"description":"","name":"veterinarianLiability","qid":"41","text":"VETERINARIAN LIABILITY AND CARE: Owner agrees to Three Tails, LLC to obtainmedical treatment for owner’s dog(s) if he\u002Fshe appears ill, injured, or exhibits any otherbehavior that would reasonably suggest that dog(s) may need medical treatmentincluding anesthesia. Owner agrees to be fully responsible for the cost of any suchmedical treatment. Owner gives permission to Three Tails, LLC to use owner’s vet ornearest 24-hour vet hospital for required treatment.","type":"control_checkbox"},{"name":"divider","qid":"42","type":"control_divider"},null,null,{"description":"","name":"nameOf45","qid":"45","subLabel":"","text":"Name of Owner","type":"control_textbox"},null,{"description":"","name":"date46","qid":"47","text":"Date","type":"control_datetime"},null,null,{"name":"divider48","qid":"50","type":"control_divider"},{"description":"","name":"signatureOf","qid":"51","subLabel":"","text":"Signature of Owner","type":"control_textbox"},{"description":"","name":"typeYour","qid":"52","subLabel":"","text":"Type your name here to acknowledge. ","type":"control_textbox"}]);
   setTimeout(function() {
JotForm.paymentExtrasOnTheFly([null,{"name":"threeTails","qid":"1","text":"Three Tails Rainbow Protocol","type":"control_head"},{"description":"","name":"petsName","qid":"2","subLabel":"","text":"Pet's Name:","type":"control_textbox"},{"description":"","name":"petGuardian","qid":"3","subLabel":"","text":"Pet Guardian:","type":"control_textbox"},{"description":"","name":"emergencyContact","qid":"4","subLabel":"","text":"Emergency Contact:","type":"control_textbox"},{"name":"ltpgtltspanStylecolor","qid":"5","text":"Three Tails Pet Sitting’s policy is to have on file a Rainbow Protocol for senior dogs (10or older) and\u002For dogs with medical\u002Fphysical conditions that could be life threatening.This is a precautionary measure to assist us in following your wishes in the unlikelyevent your pet passes away while in our care. You can provide us with an emergencycontact who will assume the responsibility to pick up your loved one and follow throughon your arrangements. Or you can give Three Tails Pet Sitting the responsibility ofmaking arrangements for your pet at your veterinarian’s office and we will arrangetransportation there and pick up ashes for you.","type":"control_text"},null,{"description":"","name":"number","qid":"7","subLabel":"","text":"Number:","type":"control_textbox"},null,null,{"description":"","name":"date","qid":"10","text":"Date:","type":"control_datetime"},{"description":"","name":"witness","qid":"11","subLabel":"","text":"Witness:","type":"control_textbox"},{"description":"","labelText":"","name":"image2","qid":"12","text":"image2","type":"control_image"},null,null,null,{"description":"","labelText":"","name":"image3","qid":"16","text":"image3","type":"control_image"},{"name":"ltpgtltspanStylecolor17","qid":"17","text":"8)WEAKENED IMMUNE SYSTEM: Owner understands special-needs dogs, young puppies, and senior dogs naturally have a higher risk of injury, stress-related illnesses, weakened immune system, or exacerbation of any pre-existing condition. As such, by using Three Tails, LLC for boarding the owner is waiving any claim for injury or illness experienced by owner’s dog while in Three Tails, LLC care. 9)Owner further agrees to indemnify, defend, and hold Three Tails, LLC harmless against any claims by persons or entities other than owner related to owner’s dog(s) participation.\nOwner has read this agreement and understands that it is an agreement to indemnify Three Tails, LLC and the other released parties and it is a waiver and release of liability and a promise not to sue or make a claim. Owner is aware that this agreement is a contract between Three Tails, LLC and owner, and that owner cannot participate in Three Tails, LLC services unless owner signs this agreement, which owner is doing of free will.","type":"control_text"},null,{"description":"","name":"date19","qid":"19","text":"Date","type":"control_datetime"},{"description":"","name":"nameOf","qid":"20","subLabel":"","text":"Name of Owner (please print)","type":"control_textbox"},{"name":"ltpgtltspanStylecolor21","qid":"21","text":"3931 Lillian Guest Circle, Valdosta, GA 31605 | (229)221-0144 | threetailspets@gmail.com","type":"control_text"},{"name":"submit","qid":"22","text":"Submit","type":"control_button"},{"description":"","name":"iWant23","qid":"23","text":"I want my emergency contact to be called and to pick up my pet if they pass before my return. (however, if your emergency contact is unreachable I understand that Three Tails Pet Sitting will transport my pet to my veterinarian’s office on my behalf and the expenses will added to my bill)","type":"control_radio"},null,{"description":"","name":"doYou25","qid":"25","text":"Do you want us to contact you in the unlikely event your dog passes away while in our care? If yes please give us a number to reach you.","type":"control_checkbox"},null,null,null,{"description":"","name":"iAuthorize","qid":"29","text":"I authorize Three Tails Pet Sitting to transport my pet to myveterinarian’s office for cremation and wish to have my ashes returned.","type":"control_checkbox"},{"description":"","name":"iAuthorize30","qid":"30","text":"I authorize Three Tails Pet Sitting to transport my pet to myveterinarian’s office for cremation and do NOT wish to have my ashes returned.","type":"control_checkbox"},{"name":"input31","qid":"31","text":"By signing below you understand Three Tails Pet Sitting Rainbow Protocol and thatthere are fees associated with this. You give Three Tails Pet Sitting permission to followyour directions as stated above.","type":"control_text"},null,{"name":"input32","qid":"33","text":"This agreement is entered into by and between Three Tails, LLC, and dog owner.","type":"control_text"},{"description":"","name":"dogsName","qid":"34","subLabel":"","text":"Dog(s) name","type":"control_textbox"},null,null,{"name":"input35","qid":"37","text":"\n\nI (owner) represent that I am the legal owner of the above named dog(s) and I assume all risks, dangers, and responsibility for injuries to the named dog(s). Owner understands and agrees that owner is solely responsible for any harm while owner's dog(s) is\u002Fare attending boarding.\n\n\nOwner is aware that owner(s) dog(s) will co-mingle with dogs owned by different owners in a supervised environment unless otherwise specified during the consultation.\n\n\nPHOTO AND VIDEO RELEASE: Three Tails, LLC loves to post pictures and videos on Facebook and\u002For Instagram. Owner agrees to allow Three Tails, LLC to use owner’s pet’s name and any images or likeness of owner’s pet taken while he\u002Fshe is at ThreeTails, LLC, in any form, for use at any time, in any media, marketing, advertising, illustration, trade or promotional materials without compensation, and owner releases to Three Tails, LLC all rights that owner may possess or claim to such image, likeness, recording, etc.\n\n\nDOG’S HEALTH: Owner further understands and agrees that owner’s dog(s) are healthy and will at times while attending Three Tails, LLC have current vaccinations for Rabies, Distemper, Parvo, and Bordetella. Owner further understands that even if owner’s dog(s)is vaccinated for Bordetella (Kennel Cough) there is a chance that the owner’s dog(s) can still contract Kennel Cough. Owner agrees that owner will not hold Three Tails, LLC responsible if owner’s dog(s) contracts Kennel Cough or other dog-dog transmitted ailments.\n\n\nThree Tails, LLC will make every effort to keep all dogs safe however owner recognizes that there are inherent risks of illness or injury when dealing with animals. Such risks include, but are not limited to problems resulting from rough play and Kennel Cough.\n\n\nRELEASE OF LIABILITY: Owner understands and agrees that during normal dog play, owner’s dog may sustain injuries. Dog play is monitored by Three Tails, LLC to best avoid injury but scratches, punctures, torn ligaments, and other injuries may occur despite the best supervision. Owner further understands and agrees that neither boarding nor Three Tails, LLC owner, Hannah White will be liable for any illness, injury,death, and\u002For escape of owner’s dog(s) provided that reasonable care and precautions are followed, and owner hereby releases Three Tails, LLC owner, Hannah White of any liability of any kind whatsoever arising from or as a result of owner’s dog(s) attending Three Tails, LLC. OWNER IS AWARE AND UNDERSTANDS THREE TAILS, LLC DOES NOT CARRY LIABILITY INSURANCE.\n\n\nWEAKENED IMMUNE SYSTEM: Owner understands special-needs dogs, young puppies, and senior dogs naturally have a higher risk of injury, stress-related illnesses, weakened immune system, or exacerbation of any pre-existing condition. As such, by using Three Tails, LLC for boarding the owner is waiving any claim for injury or illness experienced by owner’s dog while in Three Tails, LLC care.\n\n\nOwner further agrees to indemnify, defend, and hold Three Tails, LLC harmless against any claims by persons or entities other than owner related to owner’s dog(s) participation.\n\n","type":"control_text"},null,null,null,{"description":"","name":"veterinarianLiability","qid":"41","text":"VETERINARIAN LIABILITY AND CARE: Owner agrees to Three Tails, LLC to obtainmedical treatment for owner’s dog(s) if he\u002Fshe appears ill, injured, or exhibits any otherbehavior that would reasonably suggest that dog(s) may need medical treatmentincluding anesthesia. Owner agrees to be fully responsible for the cost of any suchmedical treatment. Owner gives permission to Three Tails, LLC to use owner’s vet ornearest 24-hour vet hospital for required treatment.","type":"control_checkbox"},{"name":"divider","qid":"42","type":"control_divider"},null,null,{"description":"","name":"nameOf45","qid":"45","subLabel":"","text":"Name of Owner","type":"control_textbox"},null,{"description":"","name":"date46","qid":"47","text":"Date","type":"control_datetime"},null,null,{"name":"divider48","qid":"50","type":"control_divider"},{"description":"","name":"signatureOf","qid":"51","subLabel":"","text":"Signature of Owner","type":"control_textbox"},{"description":"","name":"typeYour","qid":"52","subLabel":"","text":"Type your name here to acknowledge. ","type":"control_textbox"}]);}, 20); 
</script>
<link href="https://cdn.jotfor.ms/static/formCss.css?3.3.18236" rel="stylesheet" type="text/css" />
<link type="text/css" media="print" rel="stylesheet" href="https://cdn.jotfor.ms/css/printForm.css?3.3.18236" />
<link type="text/css" rel="stylesheet" href="https://cdn.jotfor.ms/css/styles/nova.css?3.3.18236" />
<link type="text/css" rel="stylesheet" href="https://cdn.jotfor.ms/themes/CSS/5ca4930530899c64ff77cfa1.css?"/>
<link type="text/css" rel="stylesheet" href="https://cdn.jotfor.ms/css/styles/payment/payment_styles.css?3.3.18236" />
<link type="text/css" rel="stylesheet" href="https://cdn.jotfor.ms/css/styles/payment/payment_feature.css?3.3.18236" />
</head>

<style type="text/css">


    .form-label-left{
        width:600px;
    }
    .form-line{
        padding-top:12px;
        padding-bottom:12px;
    }
    .form-label-right{
        width:600px;
    }
    .form-all{
        width:700px;
        color:rgb(28, 38, 67) !important;
        font-family:'Roboto';
        font-size:15px;
    }
</style>

<style type="text/css" id="form-designer-style">
    /* Injected CSS Code */
@import "https://fonts.googleapis.com/css?family=Roboto:light,lightitalic,normal,italic,bold,bolditalic";
.form-all:after {
  content: "";
  display: table;
  clear: both;
}
.form-all {
  font-family: "Roboto", sans-serif;
}
.form-all {
  width: 700px;
}
.form-label-left,
.form-label-right {
  width: 600px;
}
.form-label {
  white-space: normal;
}

.form-label-left {
  display: inline-block;
  white-space: normal;
  float: left;
  text-align: left;
}
.form-label-right {
  display: inline-block;
  white-space: normal;
  float: left;
  text-align: right;
}
.form-label-top {
  white-space: normal;
  display: block;
  float: none;
  text-align: left;
}
.form-radio-item label:before {
  top: 0;
}
.form-all {
  font-size: 15px;
}
.form-label {
  font-weight: bold;
}
.form-checkbox-item label,
.form-radio-item label {
  font-weight: normal;
}
.supernova {
  background-color: #ffffff;
  background-color: #eeeff1;
}
.supernova body {
  background-color: transparent;
}
/*
@width30: (unit(@formWidth, px) + 60px);
@width60: (unit(@formWidth, px)+ 120px);
@width90: (unit(@formWidth, px)+ 180px);
*/
/* | */
@media screen and (min-width: 480px) {
  .supernova .form-all {
    border: 1px solid #d2d5da;
    -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
    box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
  }
}
/* | */
/* | */
@media screen and (max-width: 480px) {
  .jotform-form .form-all {
    margin: 0;
    width: 100%;
  }
}
/* | */
/* | */
@media screen and (min-width: 480px) and (max-width: 767px) {
  .jotform-form .form-all {
    margin: 0;
    width: 100%;
  }
}
/* | */
/* | */
@media screen and (min-width: 480px) and (max-width: 699px) {
  .jotform-form .form-all {
    margin: 0;
    width: 100%;
  }
}
/* | */
/* | */
@media screen and (min-width: 768px) {
  .jotform-form {
    padding: 60px 0;
  }
}
/* | */
/* | */
@media screen and (max-width: 699px) {
  .jotform-form .form-all {
    margin: 0;
    width: 100%;
  }
}
/* | */
.supernova .form-all,
.form-all {
  background-color: #ffffff;
  border: 1px solid transparent;
}
.form-header-group {
  border-color: #e6e6e6;
}
.form-matrix-table tr {
  border-color: #e6e6e6;
}
.form-matrix-table tr:nth-child(2n) {
  background-color: #f2f2f2;
}
.form-all {
  color: #1c2643;
}
.form-header-group .form-header {
  color: #1c2643;
}
.form-header-group .form-subHeader {
  color: #2b3a67;
}
.form-sub-label {
  color: #2b3a67;
}
.form-label-top,
.form-label-left,
.form-label-right,
.form-html {
  color: #1c2643;
}
.form-checkbox-item label,
.form-radio-item label {
  color: #2b3a67;
}
.form-line.form-line-active {
  -webkit-transition-property: all;
  -moz-transition-property: all;
  -ms-transition-property: all;
  -o-transition-property: all;
  transition-property: all;
  -webkit-transition-duration: 0.3s;
  -moz-transition-duration: 0.3s;
  -ms-transition-duration: 0.3s;
  -o-transition-duration: 0.3s;
  transition-duration: 0.3s;
  -webkit-transition-timing-function: ease;
  -moz-transition-timing-function: ease;
  -ms-transition-timing-function: ease;
  -o-transition-timing-function: ease;
  transition-timing-function: ease;
  background-color: #ffffe0;
}
/* omer */
.form-radio-item,
.form-checkbox-item {
  padding-bottom: 0px !important;
}
.form-radio-item:last-child,
.form-checkbox-item:last-child {
  padding-bottom: 0;
}
/* omer */
.form-single-column .form-checkbox-item,
.form-single-column .form-radio-item {
  width: 100%;
}
.form-checkbox-item .editor-container div,
.form-radio-item .editor-container div {
  position: relative;
}
.form-checkbox-item .editor-container div:before,
.form-radio-item .editor-container div:before {
  display: inline-block;
  vertical-align: middle;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  left: 0;
  width: 18px;
  height: 18px;
}
.form-radio-item:not(#foo) {
  position: relative;
}
.form-radio-item:not(#foo) .form-radio-other.form-radio {
  display: none !important;
}
.form-radio-item:not(#foo) input[type="checkbox"],
.form-radio-item:not(#foo) input[type="radio"] {
  opacity: 0;
  width: 0;
  margin: 0;
  padding: 0;
}
.form-radio-item:not(#foo) .form-radio-other,
.form-radio-item:not(#foo) .form-checkbox-other {
  display: inline-block !important;
  margin-left: 17px;
  margin-right: 13px;
  margin-top: 0px;
}
.form-radio-item:not(#foo) .form-checkbox-other-input,
.form-radio-item:not(#foo) .form-radio-other-input {
  margin: 0;
}
.form-radio-item:not(#foo) label {
  line-height: 18px;
  float: left;
  margin-left: 37px;
  outline: none !important;
}
.form-radio-item:not(#foo) label:before {
  content: '';
  position: absolute;
  display: inline-block;
  vertical-align: baseline;
  margin-right: 4px;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  left: 4px;
  width: 18px;
  height: 18px;
  cursor: pointer;
}
.form-radio-item:not(#foo) label:after {
  content: '';
  position: absolute;
  z-index: 10;
  display: inline-block;
  opacity: 0;
  top: 5px;
  left: 9px;
  width: 8px;
  height: 8px;
}
.form-radio-item:not(#foo) input:checked + label:after {
  opacity: 1;
}
.form-radio-item:not(#foo) label:before {
  background-color: #444;
}
.form-radio-item:not(#foo) .editor-container div:before {
  background-color: #444;
  border-radius: 50%;
  content: '';
  margin: 0 4px 0 -6px;
}
.form-radio-item:not(#foo) label:after {
  background-color: #ffffff;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  cursor: pointer;
}
.form-checkbox-item:not(#foo) {
  position: relative;
}
.form-checkbox-item:not(#foo) .form-checkbox-other.form-checkbox {
  display: none !important;
}
.form-checkbox-item:not(#foo) input[type="checkbox"],
.form-checkbox-item:not(#foo) input[type="radio"] {
  opacity: 0;
  width: 0;
  margin: 0;
  padding: 0;
}
.form-checkbox-item:not(#foo) .form-radio-other,
.form-checkbox-item:not(#foo) .form-checkbox-other {
  display: inline-block !important;
  margin-left: 17px;
  margin-right: 13px;
  margin-top: 0px;
}
.form-checkbox-item:not(#foo) .form-checkbox-other-input,
.form-checkbox-item:not(#foo) .form-radio-other-input {
  margin: 0;
}
.form-checkbox-item:not(#foo) label {
  line-height: 18px;
  float: left;
  margin-left: 37px;
  outline: none !important;
}
.form-checkbox-item:not(#foo) label:before {
  content: '';
  position: absolute;
  display: inline-block;
  vertical-align: baseline;
  margin-right: 4px;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  left: 4px;
  width: 18px;
  height: 18px;
  cursor: pointer;
}
.form-checkbox-item:not(#foo) label:after {
  content: '';
  position: absolute;
  z-index: 10;
  display: inline-block;
  opacity: 0;
  top: 8px;
  left: 9px;
  width: 3px;
  height: 3px;
}
.form-checkbox-item:not(#foo) input:checked + label:after {
  opacity: 1;
}
.form-checkbox-item:not(#foo) label:before {
  background-color: #444;
}
.form-checkbox-item:not(#foo) .editor-container div:before {
  background-color: #444;
  border-radius: 50%;
  content: '';
  margin: 0 4px 0 -6px;
}
.form-checkbox-item:not(#foo) label:after {
  background-color: #ffffff;
  box-shadow: 0 2px 0 0 #ffffff, 2px 2px 0 0 #ffffff, 4px 2px 0 0 #ffffff, 6px 2px 0 0 #ffffff;
  transform: rotate(-45deg);
}
.supernova {
  height: 100%;
  background-repeat: no-repeat;
  background-attachment: scroll;
  background-position: center top;
  background-repeat: repeat;
}
.supernova {
  background-image: none;
}
#stage {
  background-image: none;
}
/* | */
.form-all {
  background-repeat: no-repeat;
  background-attachment: scroll;
  background-position: center top;
  background-repeat: repeat;
}
.form-header-group {
  background-repeat: no-repeat;
  background-attachment: scroll;
  background-position: center top;
}
.form-line {
  margin-top: 12px;
  margin-bottom: 12px;
}
.form-line {
  padding: 1px 36px;
}
.form-all .form-textbox,
.form-all .form-radio-other-input,
.form-all .form-checkbox-other-input,
.form-all .form-captcha input,
.form-all .form-spinner input,
.form-all .form-pagebreak-back,
.form-all .form-pagebreak-next,
.form-all .qq-upload-button,
.form-all .form-error-message {
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
}
.form-all .form-sub-label {
  margin-left: 3px;
}
.form-all .form-textarea {
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
}
.form-all {
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
}
.form-section:first-child {
  -webkit-border-radius: 6px 6px 0 0;
  -moz-border-radius: 6px 6px 0 0;
  border-radius: 6px 6px 0 0;
}
.form-section:last-child {
  -webkit-border-radius: 0 0 6px 6px;
  -moz-border-radius: 0 0 6px 6px;
  border-radius: 0 0 6px 6px;
}
.form-all .qq-upload-button,
.form-all .form-submit-button,
.form-all .form-submit-reset,
.form-all .form-submit-print {
  font-size: 1.15em;
  padding: 12px 18px;
  font-family: "Roboto", sans-serif;
  font-size: 15px;
  font-weight: normal;
}
.form-all .form-submit-print {
  margin-left: 0 !important;
  margin-right: 0 !important;
}
.form-all .form-pagebreak-back,
.form-all .form-pagebreak-next {
  font-size: 1em;
  padding: 9px 15px;
  font-family: "Roboto", sans-serif;
  font-size: 15px;
  font-weight: normal;
}
.form-all .form-pagebreak-back,
.form-all .form-pagebreak-next {
  box-shadow: none;
  text-shadow: none;
  color: #3e569b !important;
  background: ;
}
/*
& when ( @buttonFontType = google ) {
	@import (css) "@{buttonFontLink}";
}
*/
h2.form-header {
  line-height: 1.618em;
}
h2 ~ .form-subHeader {
  line-height: 1.5em;
}
.form-header-group {
  text-align: left;
}
/*.form-dropdown,
.form-radio-item,
.form-checkbox-item,
.form-radio-other-input,
.form-checkbox-other-input,*/
.form-captcha input,
.form-spinner input,
.form-error-message {
  padding: 4px 3px 2px 3px;
}
.form-header-group {
  font-family: "Roboto", sans-serif;
}
.form-section {
  padding: 0px 0px 0px 0px;
}
.form-header-group {
  margin: 12px 36px 12px 36px;
}
.form-header-group {
  padding: 24px 0px 24px 0px;
}
.form-header-group .form-header,
.form-header-group .form-subHeader {
  color: #2a3963;
}
.form-header-group {
  background-color: ;
}
.form-textbox,
.form-textarea {
  border-color: #dee2ed;
  padding: 9px 5px 7px 5px;
}
.form-textbox,
.form-textarea,
.form-radio-other-input,
.form-checkbox-other-input,
.form-captcha input,
.form-spinner input {
  background-color: #ffffff;
}
.form-textbox,
.form-textarea {
  width: 100%;
  max-width: 370px;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}
[data-type="control_textbox"] .form-input,
[data-type="control_textarea"] .form-input,
[data-type="control_fullname"] .form-input,
[data-type="control_phone"] .form-input,
[data-type="control_datetime"] .form-input,
[data-type="control_address"] .form-input,
[data-type="control_email"] .form-input,
[data-type="control_passwordbox"] .form-input,
[data-type="control_autocomp"] .form-input,
[data-type="control_textbox"] .form-input-wide,
[data-type="control_textarea"] .form-input-wide,
[data-type="control_fullname"] .form-input-wide,
[data-type="control_phone"] .form-input-wide,
[data-type="control_datetime"] .form-input-wide,
[data-type="control_address"] .form-input-wide,
[data-type="control_email"] .form-input-wide,
[data-type="control_passwordbox"] .form-input-wide,
[data-type="control_autocomp"] .form-input-wide {
  width: 100%;
  max-width: 370px;
}
[data-type="control_fullname"] .form-sub-label-container {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  width: 48%;
  margin-right: 0;
  float: left;
}
[data-type="control_fullname"] .form-sub-label-container:first-child {
  margin-right: 4%;
}
[data-type="control_phone"] .form-sub-label-container {
  width: 62.5%;
  margin-left: 2.5%;
  margin-right: 0;
  float: left;
  position: relative;
}
[data-type="control_phone"] .form-sub-label-container:first-child {
  width: 32.5%;
  margin-right: 2.5%;
  margin-left: 0;
}
[data-type="control_phone"] .form-sub-label-container:first-child [data-component=areaCode] {
  width: 93%;
  float: left;
}
[data-type="control_phone"] .form-sub-label-container:first-child [data-component=areaCode] ~ .form-sub-label {
  display: inline-block;
}
[data-type="control_phone"] .form-sub-label-container:first-child .phone-separate {
  position: absolute;
  top: 0;
  right: -16%;
  width: 24%;
  text-align: center;
  text-indent: -4px;
}
[data-type="control_phone"] .form-sub-label-container .date-separate {
  visibility: hidden;
}
.form-matrix-table {
  width: 100%;
  max-width: 370px;
}
.form-address-table {
  width: 100%;
  max-width: 370px;
}
.form-address-table td .form-dropdown {
  width: 100%;
}
.form-address-table td .form-sub-label-container {
  width: 96%;
}
.form-address-table td:last-child .form-sub-label-container {
  margin-left: 4%;
}
.form-address-table td[colspan="2"] .form-sub-label-container {
  width: 100%;
  margin: 0;
}
[data-type="control_dropdown"] .form-input,
[data-type="control_dropdown"] .form-input-wide {
  width: 150px;
}
.form-buttons-wrapper {
  margin-left: 0 !important;
  text-align: center;
}
.form-pagebreak {
  text-align: center !important;
}
.form-pagebreak-back-container,
.form-pagebreak-next-container {
  float: none !important;
  display: inline-block !important;
}
.form-header-group {
  border-bottom: none;
}
.form-label {
  margin-bottom: 3px;
  margin-right: 0;
}
.form-label {
  font-family: "Roboto", sans-serif;
}
li[data-type="control_image"] div {
  text-align: left;
}
li[data-type="control_image"] img {
  border: none;
  border-width: 0px !important;
  border-style: solid !important;
  border-color: false !important;
}
.form-line-column {
  width: auto;
}
.form-line-error {
  overflow: hidden;
  -webkit-transition-property: none;
  -moz-transition-property: none;
  -ms-transition-property: none;
  -o-transition-property: none;
  transition-property: none;
  -webkit-transition-duration: 0.3s;
  -moz-transition-duration: 0.3s;
  -ms-transition-duration: 0.3s;
  -o-transition-duration: 0.3s;
  transition-duration: 0.3s;
  -webkit-transition-timing-function: ease;
  -moz-transition-timing-function: ease;
  -ms-transition-timing-function: ease;
  -o-transition-timing-function: ease;
  transition-timing-function: ease;
  background-color: #fff4f4;
}
.form-line-error .form-error-message {
  background-color: #ff3200;
  clear: both;
  float: none;
}
.form-line-error .form-error-message .form-error-arrow {
  border-bottom-color: #ff3200;
}
.form-line-error input:not(#coupon-input),
.form-line-error textarea,
.form-line-error .form-validation-error {
  border: 1px solid #ff3200;
  -webkit-box-shadow: 0 0 3px #ff3200;
  -moz-box-shadow: 0 0 3px #ff3200;
  box-shadow: 0 0 3px #ff3200;
}
.form-collapse-table {
  border: 1px solid rgba(0, 0, 0, 0.2);
  -moz-background-clip: padding;
  -webkit-background-clip: padding;
  background-clip: padding -box;
  background-color: #ffffff;
  background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eff1f8));
  background: -webkit-linear-gradient(top, #ffffff, #eff1f8);
  background: -moz-linear-gradient(top, #ffffff, #eff1f8);
  background: -ms-linear-gradient(top, #ffffff, #eff1f8);
  background: -o-linear-gradient(top, #ffffff, #eff1f8);
  -webkit-box-shadow: 0 1px 0 rgba(255,255,255,0.5) inset, 0 1px 0 rgba(0,0,0,0.2);
  -moz-box-shadow: 0 1px 0 rgba(255,255,255,0.5) inset, 0 1px 0 rgba(0,0,0,0.2);
  box-shadow: 0 1px 0 rgba(255,255,255,0.5) inset, 0 1px 0 rgba(0,0,0,0.2);
  color: #4b61a1;
  padding-left: 24px;
  padding-right: 24px;
  padding-top: 24px;
  padding-bottom: 24px;
  height: auto;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  max-height: 60px;
  overflow: hidden;
  margin-left: 36px;
  margin-right: 36px;
}
.form-collapse-table:hover {
  background-color: #ffffff;
  background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#ffffff));
  background: -webkit-linear-gradient(top, #ffffff, #ffffff);
  background: -moz-linear-gradient(top, #ffffff, #ffffff);
  background: -ms-linear-gradient(top, #ffffff, #ffffff);
  background: -o-linear-gradient(top, #ffffff, #ffffff);
}
.form-collapse-table:active {
  -webkit-box-shadow: 0 1px 0 rgba(255,255,255,0.5) inset;
  -moz-box-shadow: 0 1px 0 rgba(255,255,255,0.5) inset;
  box-shadow: 0 1px 0 rgba(255,255,255,0.5) inset;
  background-color: #eff1f8;
  background: -webkit-gradient(linear, left top, left bottom, from(#eff1f8), to(#ffffff));
  background: -webkit-linear-gradient(top, #eff1f8, #ffffff);
  background: -moz-linear-gradient(top, #eff1f8, #ffffff);
  background: -ms-linear-gradient(top, #eff1f8, #ffffff);
  background: -o-linear-gradient(top, #eff1f8, #ffffff);
}
.form-collapse-table .form-collapse-mid {
  text-shadow: none;
}
.form-collapse-table .form-collapse-mid {
  margin-right: 0;
  margin-left: 0;
}
.form-collapse-table .form-collapse-mid {
  margin-top: 0;
  margin-bottom: 0;
}
.form-collapse-table .form-collapse-right {
  height: 24px;
  top: 50%;
}
.form-collapse-table .form-collapse-right-show {
  margin-top: -15px;
}
.form-collapse-table .form-collapse-right-hide {
  margin-top: -12px;
}
.ie-8 .form-all {
  margin-top: auto;
  margin-top: initial;
}
.ie-8 .form-all:before {
  display: none;
}
[data-type="control_clear"] {
  display: none;
}
/* | */
@media screen and (max-width: 480px), screen and (max-device-width: 767px) and (orientation: portrait), screen and (max-device-width: 415px) and (orientation: landscape) {
  .testOne {
    letter-spacing: 0;
  }
  .form-all {
    border: 0;
    max-width: initial;
  }
  .form-sub-label-container {
    width: 100%;
    margin: 0;
    margin-right: 0;
    float: left;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
  }
  span.form-sub-label-container + span.form-sub-label-container {
    margin-right: 0;
  }
  .form-sub-label {
    white-space: normal;
  }
  .form-address-table td,
  .form-address-table th {
    padding: 0 1px 10px;
  }
  .form-submit-button,
  .form-submit-print,
  .form-submit-reset {
    width: 100%;
    margin-left: 0!important;
  }
  div[id*=at_] {
    font-size: 14px;
    font-weight: 700;
    height: 8px;
    margin-top: 6px;
  }
  .showAutoCalendar {
    width: 20px;
  }
  img.form-image {
    max-width: 100%;
    height: auto;
  }
  .form-matrix-row-headers {
    width: 100%;
    word-break: break-all;
    min-width: 80px;
  }
  .form-collapse-table,
  .form-header-group {
    margin: 0;
  }
  .form-collapse-table {
    height: 100%;
    display: inline-block;
    width: 100%;
  }
  .form-collapse-hidden {
    display: none !important;
  }
  .form-input {
    width: 100%;
  }
  .form-label {
    width: 100% !important;
  }
  .form-label-left,
  .form-label-right {
    display: block;
    float: none;
    text-align: left;
    width: auto!important;
  }
  .form-line,
  .form-line.form-line-column {
    padding: 2% 5%;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
  }
  input[type=text],
  input[type=email],
  input[type=tel],
  textarea {
    width: 100%;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    max-width: initial !important;
  }
  .form-radio-other-input,
  .form-checkbox-other-input {
    max-width: 55% !important;
  }
  .form-dropdown,
  .form-textarea,
  .form-textbox {
    width: 100%!important;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
  }
  .form-input,
  .form-input-wide,
  .form-textarea,
  .form-textbox,
  .form-dropdown {
    max-width: initial!important;
  }
  .form-checkbox-item:not(#foo),
  .form-radio-item:not(#foo) {
    width: 100%;
  }
  .form-address-city,
  .form-address-line,
  .form-address-postal,
  .form-address-state,
  .form-address-table,
  .form-address-table .form-sub-label-container,
  .form-address-table select,
  .form-input {
    width: 100%;
  }
  div.form-header-group {
    padding: 24px 0px !important;
    margin: 0 12px 2% !important;
    margin-left: 5%!important;
    margin-right: 5%!important;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
  }
  div.form-header-group.hasImage img {
    max-width: 100%;
  }
  [data-type="control_button"] {
    margin-bottom: 0 !important;
  }
  [data-type=control_fullname] .form-sub-label-container {
    width: 48%;
  }
  [data-type=control_fullname] .form-sub-label-container:first-child {
    margin-right: 4%;
  }
  [data-type=control_phone] .form-sub-label-container {
    width: 65%;
    margin-right: 0;
    margin-left: 0;
    float: left;
  }
  [data-type=control_phone] .form-sub-label-container:first-child {
    width: 31%;
    margin-right: 4%;
  }
  [data-type=control_datetime] .allowTime-container {
    width: 100%;
  }
  [data-type=control_datetime] .allowTime-container .form-sub-label-container {
    width: 24%!important;
    margin-left: 6%;
    margin-right: 0;
  }
  [data-type=control_datetime] .allowTime-container .form-sub-label-container:first-child {
    margin-left: 0;
  }
  [data-type=control_datetime] span + span + span > span:first-child {
    display: block;
    width: 100% !important;
  }
  [data-type=control_birthdate] .form-sub-label-container,
  [data-type=control_time] .form-sub-label-container {
    width: 27.3%!important;
    margin-right: 6% !important;
  }
  [data-type=control_time] .form-sub-label-container:last-child {
    width: 33.3%!important;
    margin-right: 0 !important;
  }
  .form-pagebreak-back-container,
  .form-pagebreak-next-container {
    min-height: 1px;
    width: 50% !important;
  }
  .form-pagebreak-back,
  .form-pagebreak-next,
  .form-product-item.hover-product-item {
    width: 100%;
  }
  .form-pagebreak-back-container {
    padding: 0;
    text-align: right;
  }
  .form-pagebreak-next-container {
    padding: 0;
    text-align: left;
  }
  .form-pagebreak {
    margin: 0 auto;
  }
  .form-buttons-wrapper {
    margin: 0!important;
    margin-left: 0!important;
  }
  .form-buttons-wrapper button {
    width: 100%;
  }
  .form-buttons-wrapper .form-submit-print {
    margin: 0 !important;
  }
  table {
    width: 100%!important;
    max-width: initial!important;
  }
  table td + td {
    padding-left: 3%;
  }
  .form-checkbox-item,
  .form-radio-item {
    white-space: normal!important;
  }
  .form-checkbox-item input,
  .form-radio-item input {
    width: auto;
  }
  .form-collapse-table {
    margin: 0 5%;
    display: block;
    zoom: 1;
    width: auto;
  }
  .form-collapse-table:before,
  .form-collapse-table:after {
    display: table;
    content: '';
    line-height: 0;
  }
  .form-collapse-table:after {
    clear: both;
  }
  .fb-like-box {
    width: 98% !important;
  }
  .form-error-message {
    clear: both;
    bottom: -10px;
  }
  .date-separate,
  .phone-separate {
    display: none;
  }
  .custom-field-frame,
  .direct-embed-widgets,
  .signature-pad-wrapper {
    width: 100% !important;
  }
}
/* | */

/*__INSPECT_SEPERATOR__*/
#id_8 {
    margin-top : none;
}

.form-line {
    margin-top : none;
}

p {
    margin-top : none;
    padding : none;
}

.form-html span {

}

.form-html {
    margin-top : none;
}

.form-checkbox-item label {
    line-height : -3px;
}

.form-input-wide {

}

#label_input_6_1 {

}

.form-section.page-section {

}

.form-label.form-label-auto {
        
      display: block;
      float: none;
      text-align: left;
      width: 100%;
  }
    /* Injected CSS Code */
</style>

<body>

<form class="jotform-form" action="https://submit.jotform.com/submit/201559273014046/" method="post" name="form_201559273014046" id="201559273014046" accept-charset="utf-8" autocomplete="on">
  <input type="hidden" name="formID" value="201559273014046" />
  <input type="hidden" id="JWTContainer" value="" />
  <input type="hidden" id="cardinalOrderNumber" value="" />
  <div role="main" class="form-all">
    <ul class="form-section page-section">
      <li id="cid_1" class="form-input-wide" data-type="control_head">
        <div style="display:table;width:100%">
          <div class="form-header-group hasImage header-large" data-imagealign="Top">
            <div class="header-logo">
              <img src="https://www.jotform.com/uploads/guest_fd9cf9ee26dde9a1/form_files/image_5ed9b7e4928ab.png?nc=1" alt="" width="241.5" class="header-logo-top" />
            </div>
            <div class="header-text httac htvam">
              <h1 id="header_1" class="form-header" data-component="header">
                Three Tails Rainbow Protocol
              </h1>
            </div>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_textbox" id="id_2">
        <label class="form-label form-label-top form-label-auto" id="label_2" for="input_2"> Pet's Name: </label>
        <div id="cid_2" class="form-input-wide">
          <input type="text" id="input_2" name="q2_petsName" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_2" />
        </div>
      </li>
      <li class="form-line" data-type="control_textbox" id="id_3">
        <label class="form-label form-label-top form-label-auto" id="label_3" for="input_3"> Pet Guardian: </label>
        <div id="cid_3" class="form-input-wide">
          <input type="text" id="input_3" name="q3_petGuardian" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_3" />
        </div>
      </li>
      <li class="form-line" data-type="control_textbox" id="id_4">
        <label class="form-label form-label-top form-label-auto" id="label_4" for="input_4"> Emergency Contact: </label>
        <div id="cid_4" class="form-input-wide">
          <input type="text" id="input_4" name="q4_emergencyContact" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_4" />
        </div>
      </li>
      <li class="form-line" data-type="control_text" id="id_5">
        <div id="cid_5" class="form-input-wide">
          <div id="text_5" class="form-html" data-component="text">
            <p>Three Tails Pet Sitting’s policy is to have on file a Rainbow Protocol for senior dogs (10<br />or older) and/or dogs with medical/physical conditions that could be life threatening.<br />This is a precautionary measure to assist us in following your wishes in the unlikely<br />event your pet passes away while in our care. You can provide us with an emergency<br />contact who will assume the responsibility to pick up your loved one and follow through<br />on your arrangements. Or you can give Three Tails Pet Sitting the responsibility of<br />making arrangements for your pet at your veterinarian’s office and we will arrange<br />transportation there and pick up ashes for you.</p>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_checkbox" id="id_25">
        <label class="form-label form-label-top form-label-auto" id="label_25" for="input_25"> Do you want us to contact you in the unlikely event your dog passes away while in our care? If yes please give us a number to reach you. </label>
        <div id="cid_25" class="form-input-wide">
          <div class="form-single-column" role="group" aria-labelledby="label_25" data-component="checkbox">
            <span class="form-checkbox-item" style="clear:left">
              <span class="dragger-item">
              </span>
              <input type="checkbox" class="form-checkbox" id="input_25_0" name="q25_doYou25[]" value="Yes" />
              <label id="label_input_25_0" for="input_25_0"> Yes </label>
            </span>
            <span class="form-checkbox-item" style="clear:left">
              <span class="dragger-item">
              </span>
              <input type="checkbox" class="form-checkbox" id="input_25_1" name="q25_doYou25[]" value="No" />
              <label id="label_input_25_1" for="input_25_1"> No </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_textbox" id="id_7">
        <label class="form-label form-label-top form-label-auto" id="label_7" for="input_7"> Number: </label>
        <div id="cid_7" class="form-input-wide">
          <input type="text" id="input_7" name="q7_number" data-type="input-textbox" class="form-textbox" size="20" value="" placeholder="555-555-5555" data-component="textbox" aria-labelledby="label_7" />
        </div>
      </li>
      <li class="form-line" data-type="control_radio" id="id_23">
        <label class="form-label form-label-top form-label-auto" id="label_23" for="input_23"> I want my emergency contact to be called and to pick up my pet if they pass before my return. (however, if your emergency contact is unreachable I understand that Three Tails Pet Sitting will transport my pet to my veterinarian’s office on my behalf and the expenses will added to my bill) </label>
        <div id="cid_23" class="form-input-wide">
          <div class="form-single-column" role="group" aria-labelledby="label_23" data-component="radio">
            <span class="form-radio-item" style="clear:left">
              <span class="dragger-item">
              </span>
              <input type="radio" class="form-radio" id="input_23_0" name="q23_iWant23" value="Yes" />
              <label id="label_input_23_0" for="input_23_0"> Yes </label>
            </span>
            <span class="form-radio-item" style="clear:left">
              <span class="dragger-item">
              </span>
              <input type="radio" class="form-radio" id="input_23_1" name="q23_iWant23" value="No" />
              <label id="label_input_23_1" for="input_23_1"> No </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_checkbox" id="id_29">
        <label class="form-label form-label-top form-label-auto" id="label_29" for="input_29"> I authorize Three Tails Pet Sitting to transport my pet to myveterinarian’s office for cremation and wish to have my ashes returned. </label>
        <div id="cid_29" class="form-input-wide">
          <div class="form-single-column" role="group" aria-labelledby="label_29" data-component="checkbox">
            <span class="form-checkbox-item" style="clear:left">
              <span class="dragger-item">
              </span>
              <input type="checkbox" class="form-checkbox" id="input_29_0" name="q29_iAuthorize[]" value="Yes" />
              <label id="label_input_29_0" for="input_29_0"> Yes </label>
            </span>
            <span class="form-checkbox-item" style="clear:left">
              <span class="dragger-item">
              </span>
              <input type="checkbox" class="form-checkbox" id="input_29_1" name="q29_iAuthorize[]" value="No" />
              <label id="label_input_29_1" for="input_29_1"> No </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_checkbox" id="id_30">
        <label class="form-label form-label-top form-label-auto" id="label_30" for="input_30"> I authorize Three Tails Pet Sitting to transport my pet to myveterinarian’s office for cremation and do NOT wish to have my ashes returned. </label>
        <div id="cid_30" class="form-input-wide">
          <div class="form-single-column" role="group" aria-labelledby="label_30" data-component="checkbox">
            <span class="form-checkbox-item" style="clear:left">
              <span class="dragger-item">
              </span>
              <input type="checkbox" class="form-checkbox" id="input_30_0" name="q30_iAuthorize30[]" value="Yes" />
              <label id="label_input_30_0" for="input_30_0"> Yes </label>
            </span>
            <span class="form-checkbox-item" style="clear:left">
              <span class="dragger-item">
              </span>
              <input type="checkbox" class="form-checkbox" id="input_30_1" name="q30_iAuthorize30[]" value="No" />
              <label id="label_input_30_1" for="input_30_1"> No </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_text" id="id_31">
        <div id="cid_31" class="form-input-wide">
          <div id="text_31" class="form-html" data-component="text">
            <p><span style="font-family: helvetica, arial, sans-serif; font-size: 12pt;">By signing below you understand Three Tails Pet Sitting Rainbow Protocol and that</span><br /><span style="font-family: helvetica, arial, sans-serif; font-size: 12pt;">there are fees associated with this. You give Three Tails Pet Sitting permission to follow</span><br /><span style="font-family: helvetica, arial, sans-serif; font-size: 12pt;">your directions as stated above.</span></p>
          </div>
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_textbox" id="id_52">
        <label class="form-label form-label-top form-label-auto" id="label_52" for="input_52">
          Type your name here to acknowledge.
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_52" class="form-input-wide jf-required">
          <input type="text" id="input_52" name="q52_typeYour" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" aria-labelledby="label_52" required="" />
        </div>
      </li>
      <li class="form-line" data-type="control_datetime" id="id_10">
        <label class="form-label form-label-top form-label-auto" id="label_10" for="lite_mode_10"> Date: </label>
        <div id="cid_10" class="form-input-wide">
          <div data-wrapper-react="true">
            <div style="display:none">
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[limitDate]" id="month_10" name="q10_date[month]" size="2" data-maxlength="2" maxLength="2" value="" autoComplete="off" aria-labelledby="label_10 sublabel_10_month" />
                <span class="date-separate" aria-hidden="true">
                   /
                </span>
                <label class="form-sub-label" for="month_10" id="sublabel_10_month" style="min-height:13px" aria-hidden="false"> Month </label>
              </span>
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[limitDate]" id="day_10" name="q10_date[day]" size="2" data-maxlength="2" maxLength="2" value="" autoComplete="off" aria-labelledby="label_10 sublabel_10_day" />
                <span class="date-separate" aria-hidden="true">
                   /
                </span>
                <label class="form-sub-label" for="day_10" id="sublabel_10_day" style="min-height:13px" aria-hidden="false"> Day </label>
              </span>
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[limitDate]" id="year_10" name="q10_date[year]" size="4" data-maxlength="4" data-age="" maxLength="4" value="" autoComplete="off" aria-labelledby="label_10 sublabel_10_year" />
                <label class="form-sub-label" for="year_10" id="sublabel_10_year" style="min-height:13px" aria-hidden="false"> Year </label>
              </span>
            </div>
            <span class="form-sub-label-container " style="vertical-align:top">
              <input type="text" class="form-textbox validate[limitDate, validateLiteDate]" id="lite_mode_10" size="12" data-maxlength="12" maxLength="12" data-age="" value="" data-format="mmddyyyy" data-seperator="/" placeholder="mm/dd/yyyy" autoComplete="off" aria-labelledby="label_10 sublabel_10_litemode" />
              <img class=" newDefaultTheme-dateIcon icon-liteMode" alt="Pick a Date" id="input_10_pick" src="https://cdn.jotfor.ms/images/calendar.png" data-component="datetime" aria-hidden="true" data-allow-time="No" data-version="v1" />
              <label class="form-sub-label" for="lite_mode_10" id="sublabel_10_litemode" style="min-height:13px" aria-hidden="false"> Date </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_textbox" id="id_11">
        <label class="form-label form-label-top form-label-auto" id="label_11" for="input_11"> Witness: </label>
        <div id="cid_11" class="form-input-wide">
          <input type="text" id="input_11" name="q11_witness" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_11" />
        </div>
      </li>
      <li class="form-line" data-type="control_image" id="id_12">
        <div id="cid_12" class="form-input-wide">
          <div style="text-align:center">
            <img alt="" class="form-image" style="border:0" src="https://www.jotform.com/uploads/guest_fd9cf9ee26dde9a1/form_files/image_5ed9b7e4f00a5.png?nc=1" height="126.75px" width="227.25px" data-component="image" />
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_text" id="id_33">
        <div id="cid_33" class="form-input-wide">
          <div id="text_33" class="form-html" data-component="text">
            <p><span style="font-family: helvetica, arial, sans-serif; font-size: 12pt;"><strong>This agreement is entered into by and between Three Tails, LLC, and dog owner.</strong></span></p>
          </div>
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_textbox" id="id_34">
        <label class="form-label form-label-top form-label-auto" id="label_34" for="input_34">
          Dog(s) name
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_34" class="form-input-wide jf-required">
          <input type="text" id="input_34" name="q34_dogsName" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" aria-labelledby="label_34" required="" />
        </div>
      </li>
      <li class="form-line" data-type="control_text" id="id_37">
        <div id="cid_37" class="form-input-wide">
          <div id="text_37" class="form-html" data-component="text">
            <ol>
              <li style="text-align: left;">
                <p style="text-align: left;"><span style="font-size: 12pt; font-family: helvetica, arial, sans-serif;">I (owner) represent that I am the legal owner of the above named dog(s) and I assume all risks, dangers, and responsibility for injuries to the named dog(s). Owner understands and agrees that owner is solely responsible for any harm while owner's dog(s) is/are attending boarding.<br /></span></p>
              </li>
              <li style="text-align: left;">
                <p><span style="font-size: 12pt; font-family: helvetica, arial, sans-serif;">Owner is aware that owner(s) dog(s) will co-mingle with dogs owned by different owners in a supervised environment unless otherwise specified during the consultation.<br /></span></p>
              </li>
              <li style="text-align: left;">
                <p><span style="font-size: 12pt; font-family: helvetica, arial, sans-serif;">PHOTO AND VIDEO RELEASE: Three Tails, LLC loves to post pictures and videos on Facebook and/or Instagram. Owner agrees to allow Three Tails, LLC to use owner’s pet’s name and any images or likeness of owner’s pet taken while he/she is at Three<br />Tails, LLC, in any form, for use at any time, in any media, marketing, advertising, illustration, trade or promotional materials without compensation, and owner releases to Three Tails, LLC all rights that owner may possess or claim to such image, likeness, recording, etc.<br /></span></p>
              </li>
              <li style="text-align: left;">
                <p><span style="font-size: 12pt; font-family: helvetica, arial, sans-serif;">DOG’S HEALTH: Owner further understands and agrees that owner’s dog(s) are healthy and will at times while attending Three Tails, LLC have current vaccinations for Rabies, Distemper, Parvo, and Bordetella. Owner further understands that even if owner’s dog(s)<br />is vaccinated for Bordetella (Kennel Cough) there is a chance that the owner’s dog(s) can still contract Kennel Cough. Owner agrees that owner will not hold Three Tails, LLC responsible if owner’s dog(s) contracts Kennel Cough or other dog-dog transmitted ailments.<br /></span></p>
              </li>
              <li style="text-align: left;">
                <p><span style="font-size: 12pt; font-family: helvetica, arial, sans-serif;">Three Tails, LLC will make every effort to keep all dogs safe however owner recognizes that there are inherent risks of illness or injury when dealing with animals. Such risks include, but are not limited to problems resulting from rough play and Kennel Cough.<br /></span></p>
              </li>
              <li style="text-align: left;">
                <p><span style="font-size: 12pt; font-family: helvetica, arial, sans-serif;">RELEASE OF LIABILITY: Owner understands and agrees that during normal dog play, owner’s dog may sustain injuries. Dog play is monitored by Three Tails, LLC to best avoid injury but scratches, punctures, torn ligaments, and other injuries may occur despite the best supervision. Owner further understands and agrees that neither boarding nor Three Tails, LLC owner, Hannah White will be liable for any illness, injury,<br />death, and/or escape of owner’s dog(s) provided that reasonable care and precautions are followed, and owner hereby releases Three Tails, LLC owner, Hannah White of any liability of any kind whatsoever arising from or as a result of owner’s dog(s) attending Three Tails, LLC. OWNER IS AWARE AND UNDERSTANDS THREE TAILS, LLC DOES NOT CARRY LIABILITY INSURANCE.<br /></span></p>
              </li>
              <li style="text-align: left;">
                <p><span style="font-size: 12pt; font-family: helvetica, arial, sans-serif;">WEAKENED IMMUNE SYSTEM: Owner understands special-needs dogs, young puppies, and senior dogs naturally have a higher risk of injury, stress-related illnesses, weakened immune system, or exacerbation of any pre-existing condition. As such, by using Three Tails, LLC for boarding the owner is waiving any claim for injury or illness experienced by owner’s dog while in Three Tails, LLC care.<br /></span></p>
              </li>
              <li style="text-align: left;">
                <p style="text-align: left;"><span style="font-size: 12pt; font-family: helvetica, arial, sans-serif;">Owner further agrees to indemnify, defend, and hold Three Tails, LLC harmless against any claims by persons or entities other than owner related to owner’s dog(s) participation.</span></p>
              </li>
            </ol>
          </div>
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_checkbox" id="id_41">
        <label class="form-label form-label-top" id="label_41" for="input_41">
          VETERINARIAN LIABILITY AND CARE: Owner agrees to Three Tails, LLC to obtainmedical treatment for owner’s dog(s) if he/she appears ill, injured, or exhibits any otherbehavior that would reasonably suggest that dog(s) may need medical treatmentincluding anesthesia. Owner agrees to be fully responsible for the cost of any suchmedical treatment. Owner gives permission to Three Tails, LLC to use owner’s vet ornearest 24-hour vet hospital for required treatment.
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_41" class="form-input-wide jf-required">
          <div class="form-multiple-column" data-columncount="2" role="group" aria-labelledby="label_41" data-component="checkbox">
            <span class="form-checkbox-item">
              <span class="dragger-item">
              </span>
              <input type="checkbox" class="form-checkbox validate[required]" id="input_41_0" name="q41_veterinarianLiability[]" value="Yes" required="" />
              <label id="label_input_41_0" for="input_41_0"> Yes </label>
            </span>
            <span class="form-checkbox-item">
              <span class="dragger-item">
              </span>
              <input type="checkbox" class="form-checkbox validate[required]" id="input_41_1" name="q41_veterinarianLiability[]" value="No" required="" />
              <label id="label_input_41_1" for="input_41_1"> No </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_divider" id="id_42">
        <div id="cid_42" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:5px">
          </div>
        </div>
      </li>
      <li class="form-line form-line-column form-col-1 jf-required" data-type="control_textbox" id="id_45">
        <label class="form-label form-label-top" id="label_45" for="input_45">
          Name of Owner
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_45" class="form-input-wide jf-required">
          <input type="text" id="input_45" name="q45_nameOf45" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" aria-labelledby="label_45" required="" />
        </div>
      </li>
      <li class="form-line form-line-column form-col-2" data-type="control_datetime" id="id_47">
        <label class="form-label form-label-top" id="label_47" for="lite_mode_47"> Date </label>
        <div id="cid_47" class="form-input-wide">
          <div data-wrapper-react="true">
            <div style="display:none">
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[disallowPast, limitDate]" id="month_47" name="q47_date46[month]" size="2" data-maxlength="2" maxLength="2" value="06" autoComplete="off" aria-labelledby="label_47 sublabel_47_month" />
                <span class="date-separate" aria-hidden="true">
                   /
                </span>
                <label class="form-sub-label" for="month_47" id="sublabel_47_month" style="min-height:13px" aria-hidden="false"> Month </label>
              </span>
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="currentDate form-textbox validate[disallowPast, limitDate]" id="day_47" name="q47_date46[day]" size="2" data-maxlength="2" maxLength="2" value="05" autoComplete="off" aria-labelledby="label_47 sublabel_47_day" />
                <span class="date-separate" aria-hidden="true">
                   /
                </span>
                <label class="form-sub-label" for="day_47" id="sublabel_47_day" style="min-height:13px" aria-hidden="false"> Day </label>
              </span>
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[disallowPast, limitDate]" id="year_47" name="q47_date46[year]" size="4" data-maxlength="4" data-age="" maxLength="4" value="2020" autoComplete="off" aria-labelledby="label_47 sublabel_47_year" />
                <label class="form-sub-label" for="year_47" id="sublabel_47_year" style="min-height:13px" aria-hidden="false"> Year </label>
              </span>
            </div>
            <span class="form-sub-label-container " style="vertical-align:top">
              <input type="text" class="form-textbox validate[disallowPast, limitDate, validateLiteDate]" id="lite_mode_47" size="12" data-maxlength="12" maxLength="12" data-age="" value="06/05/2020" data-format="mmddyyyy" data-seperator="/" placeholder="mm/dd/yyyy" autoComplete="off" aria-labelledby="label_47" />
              <img class=" newDefaultTheme-dateIcon icon-liteMode" alt="Pick a Date" id="input_47_pick" src="https://cdn.jotfor.ms/images/calendar.png" data-component="datetime" aria-hidden="true" data-allow-time="No" data-version="v1" />
              <label class="form-sub-label" for="lite_mode_47" id="sublabel_47_litemode" style="min-height:13px" aria-hidden="false">  </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_divider" id="id_50">
        <div id="cid_50" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:25px;margin-bottom:25px">
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_image" id="id_16">
        <div id="cid_16" class="form-input-wide">
          <div style="text-align:center">
            <img alt="" class="form-image" style="border:0" src="https://www.jotform.com/uploads/guest_fd9cf9ee26dde9a1/form_files/image_5ed9b7e54440d.png?nc=1" height="126.75px" width="227.25px" data-component="image" />
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_text" id="id_17">
        <div id="cid_17" class="form-input-wide">
          <div id="text_17" class="form-html" data-component="text">
            <p><span style="color: #000000;">8</span><span style="color: #000000;">)</span><span style="color: #000000;">W</span><span style="color: #000000;">E</span><span style="color: #000000;">A</span><span style="color: #000000;">K</span><span style="color: #000000;">E</span><span style="color: #000000;">N</span><span style="color: #000000;">E</span><span style="color: #000000;">D</span> <span style="color: #000000;">I</span><span style="color: #000000;">M</span><span style="color: #000000;">M</span><span style="color: #000000;">U</span><span style="color: #000000;">N</span><span style="color: #000000;">E</span> <span style="color: #000000;">S</span><span style="color: #000000;">Y</span><span style="color: #000000;">S</span><span style="color: #131313;">T</span><span style="color: #000000;">E</span><span style="color: #000000;">M</span><span style="color: #000000;">:</span> <span style="color: #000000;">O</span><span style="color: #000000;">w</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span> <span style="color: #000000;">u</span><span style="color: #000000;">n</span><span style="color: #000000;">d</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span><span style="color: #000000;">s</span><span style="color: #000000;">t</span><span style="color: #000000;">a</span><span style="color: #000000;">n</span><span style="color: #000000;">d</span><span style="color: #000000;">s</span> <span style="color: #000000;">s</span><span style="color: #000000;">p</span><span style="color: #000000;">e</span><span style="color: #000000;">c</span><span style="color: #000000;">i</span><span style="color: #000000;">a</span><span style="color: #000000;">l</span><span style="color: #000000;">-</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span><span style="color: #000000;">e</span><span style="color: #000000;">d</span><span style="color: #000000;">s</span> <span style="color: #000000;">d</span><span style="color: #000000;">o</span><span style="color: #000000;">g</span><span style="color: #000000;">s</span><span style="color: #000000;">,</span> <span style="color: #000000;">y</span><span style="color: #000000;">o</span><span style="color: #000000;">u</span><span style="color: #000000;">n</span><span style="color: #000000;">g</span> <span style="color: #000000;">p</span><span style="color: #000000;">u</span><span style="color: #000000;">p</span><span style="color: #000000;">p</span><span style="color: #000000;">i</span><span style="color: #000000;">e</span><span style="color: #000000;">s</span><span style="color: #000000;">,</span> <span style="color: #000000;">a</span><span style="color: #000000;">n</span><span style="color: #000000;">d</span> <span style="color: #000000;">s</span><span style="color: #000000;">e</span><span style="color: #000000;">n</span><span style="color: #000000;">i</span><span style="color: #000000;">o</span><span style="color: #000000;">r</span> <span style="color: #000000;">d</span><span style="color: #000000;">o</span><span style="color: #000000;">g</span><span style="color: #000000;">s</span> <span style="color: #000000;">n</span><span style="color: #000000;">a</span><span style="color: #000000;">t</span><span style="color: #000000;">u</span><span style="color: #000000;">r</span><span style="color: #000000;">a</span><span style="color: #000000;">l</span><span style="color: #000000;">l</span><span style="color: #000000;">y</span> <span style="color: #000000;">h</span><span style="color: #000000;">a</span><span style="color: #000000;">v</span><span style="color: #000000;">e</span> <span style="color: #000000;">a</span> <span style="color: #000000;">h</span><span style="color: #000000;">i</span><span style="color: #000000;">g</span><span style="color: #000000;">h</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span> <span style="color: #000000;">r</span><span style="color: #c0c0c0;">i</span><span style="color: #000000;">s</span><span style="color: #000000;">k</span> <span style="color: #000000;">o</span><span style="color: #000000;">f</span> <span style="color: #000000;">i</span><span style="color: #000000;">n</span><span style="color: #000000;">j</span><span style="color: #000000;">u</span><span style="color: #000000;">r</span><span style="color: #000000;">y</span><span style="color: #000000;">,</span> <span style="color: #000000;">s</span><span style="color: #000000;">t</span><span style="color: #000000;">r</span><span style="color: #000000;">e</span><span style="color: #000000;">s</span><span style="color: #000000;">s</span><span style="color: #000000;">-</span><span style="color: #000000;">r</span><span style="color: #000000;">e</span><span style="color: #000000;">l</span><span style="color: #000000;">a</span><span style="color: #000000;">t</span><span style="color: #000000;">e</span><span style="color: #000000;">d</span> <span style="color: #000000;">i</span><span style="color: #000000;">l</span><span style="color: #000000;">l</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span><span style="color: #000000;">s</span><span style="color: #000000;">s</span><span style="color: #000000;">e</span><span style="color: #000000;">s</span><span style="color: #000000;">,</span> <span style="color: #000000;">w</span><span style="color: #000000;">e</span><span style="color: #000000;">a</span><span style="color: #000000;">k</span><span style="color: #000000;">e</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span><span style="color: #000000;">d</span> <span style="color: #000000;">i</span><span style="color: #000000;">m</span><span style="color: #000000;">m</span><span style="color: #000000;">u</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span> <span style="color: #000000;">s</span><span style="color: #000000;">y</span><span style="color: #000000;">s</span><span style="color: #000000;">t</span><span style="color: #000000;">e</span><span style="color: #000000;">m</span><span style="color: #000000;">,</span> <span style="color: #000000;">o</span><span style="color: #000000;">r</span> <span style="color: #000000;">e</span><span style="color: #000000;">x</span><span style="color: #000000;">a</span><span style="color: #000000;">c</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span><span style="color: #000000;">b</span><span style="color: #000000;">a</span><span style="color: #000000;">t</span><span style="color: #fefefe;">i</span><span style="color: #000000;">o</span><span style="color: #000000;">n</span> <span style="color: #000000;">o</span><span style="color: #000000;">f</span> <span style="color: #000000;">a</span><span style="color: #000000;">n</span><span style="color: #000000;">y</span> <span style="color: #000000;">p</span><span style="color: #000000;">r</span><span style="color: #000000;">e</span><span style="color: #000000;">-</span><span style="color: #000000;">e</span><span style="color: #000000;">x</span><span style="color: #c7c7c7;">i</span><span style="color: #000000;">s</span><span style="color: #000000;">t</span><span style="color: #fefefe;">i</span><span style="color: #000000;">n</span><span style="color: #000000;">g</span> <span style="color: #000000;">c</span><span style="color: #000000;">o</span><span style="color: #000000;">n</span><span style="color: #000000;">d</span><span style="color: #000000;">i</span><span style="color: #000000;">t</span><span style="color: #000000;">i</span><span style="color: #000000;">o</span><span style="color: #000000;">n</span><span style="color: #000000;">.</span> <span style="color: #000000;">A</span><span style="color: #000000;">s</span> <span style="color: #000000;">s</span><span style="color: #000000;">u</span><span style="color: #000000;">c</span><span style="color: #000000;">h</span><span style="color: #000000;">,</span> <span style="color: #000000;">b</span><span style="color: #000000;">y</span> <span style="color: #000000;">u</span><span style="color: #000000;">s</span><span style="color: #000000;">i</span><span style="color: #000000;">n</span><span style="color: #000000;">g</span> <span style="color: #191919;">T</span><span style="color: #000000;">h</span><span style="color: #000000;">r</span><span style="color: #000000;">e</span><span style="color: #000000;">e</span> <span style="color: #060606;">T</span><span style="color: #000000;">a</span><span style="color: #000000;">i</span><span style="color: #000000;">l</span><span style="color: #000000;">s</span><span style="color: #000000;">,</span> <span style="color: #000000;">L</span><span style="color: #000000;">L</span><span style="color: #000000;">C</span> <span style="color: #000000;">f</span><span style="color: #000000;">o</span><span style="color: #000000;">r</span> <span style="color: #000000;">b</span><span style="color: #000000;">o</span><span style="color: #000000;">a</span><span style="color: #000000;">r</span><span style="color: #000000;">d</span><span style="color: #000000;">i</span><span style="color: #000000;">n</span><span style="color: #000000;">g</span> <span style="color: #000000;">t</span><span style="color: #000000;">h</span><span style="color: #000000;">e</span> <span style="color: #000000;">o</span><span style="color: #000000;">w</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span> <span style="color: #000000;">i</span><span style="color: #000000;">s</span> <span style="color: #000000;">w</span><span style="color: #000000;">a</span><span style="color: #000000;">i</span><span style="color: #000000;">v</span><span style="color: #f8f8f8;">i</span><span style="color: #000000;">n</span><span style="color: #000000;">g</span> <span style="color: #000000;">a</span><span style="color: #000000;">n</span><span style="color: #000000;">y</span> <span style="color: #000000;">c</span><span style="color: #e2e2e2;">l</span><span style="color: #000000;">a</span><span style="color: #000000;">i</span><span style="color: #000000;">m</span> <span style="color: #000000;">f</span><span style="color: #000000;">o</span><span style="color: #000000;">r</span> <span style="color: #000000;">i</span><span style="color: #000000;">n</span><span style="color: #000000;">j</span><span style="color: #000000;">u</span><span style="color: #000000;">r</span><span style="color: #000000;">y</span> <span style="color: #000000;">o</span><span style="color: #000000;">r</span> <span style="color: #000000;">i</span><span style="color: #000000;">l</span><span style="color: #000000;">l</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span><span style="color: #000000;">s</span><span style="color: #000000;">s</span> <span style="color: #000000;">e</span><span style="color: #000000;">x</span><span style="color: #000000;">p</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span><span style="color: #dfdfdf;">i</span><span style="color: #000000;">e</span><span style="color: #000000;">n</span><span style="color: #000000;">c</span><span style="color: #000000;">e</span><span style="color: #000000;">d</span> <span style="color: #000000;">b</span><span style="color: #000000;">y</span> <span style="color: #000000;">o</span><span style="color: #000000;">w</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span><span style="color: #cccccc;">’</span><span style="color: #000000;">s</span> <span style="color: #000000;">d</span><span style="color: #000000;">o</span><span style="color: #000000;">g</span> <span style="color: #000000;">w</span><span style="color: #000000;">h</span><span style="color: #000000;">i</span><span style="color: #000000;">l</span><span style="color: #000000;">e</span> <span style="color: #000000;">i</span><span style="color: #000000;">n</span> <span style="color: #000000;">T</span><span style="color: #000000;">h</span><span style="color: #000000;">r</span><span style="color: #000000;">e</span><span style="color: #000000;">e</span> <span style="color: #000000;">T</span><span style="color: #000000;">a</span><span style="color: #000000;">i</span><span style="color: #000000;">l</span><span style="color: #000000;">s</span><span style="color: #000000;">,</span> <span style="color: #000000;">L</span><span style="color: #000000;">L</span><span style="color: #000000;">C</span> <span style="color: #000000;">c</span><span style="color: #000000;">a</span><span style="color: #000000;">r</span><span style="color: #000000;">e</span><span style="color: #000000;">.</span> <span style="color: #000000;">9</span><span style="color: #000000;">)</span><span style="color: #000000;">O</span><span style="color: #000000;">w</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span> <span style="color: #000000;">f</span><span style="color: #000000;">u</span><span style="color: #000000;">r</span><span style="color: #000000;">t</span><span style="color: #000000;">h</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span> <span style="color: #000000;">a</span><span style="color: #000000;">g</span><span style="color: #000000;">r</span><span style="color: #000000;">e</span><span style="color: #000000;">e</span><span style="color: #000000;">s</span> <span style="color: #000000;">t</span><span style="color: #000000;">o</span> <span style="color: #000000;">i</span><span style="color: #000000;">n</span><span style="color: #000000;">d</span><span style="color: #000000;">e</span><span style="color: #000000;">m</span><span style="color: #000000;">n</span><span style="color: #000000;">i</span><span style="color: #000000;">f</span><span style="color: #000000;">y</span><span style="color: #000000;">,</span> <span style="color: #000000;">d</span><span style="color: #000000;">e</span><span style="color: #000000;">f</span><span style="color: #000000;">e</span><span style="color: #000000;">n</span><span style="color: #000000;">d</span><span style="color: #000000;">,</span> <span style="color: #000000;">a</span><span style="color: #000000;">n</span><span style="color: #000000;">d</span> <span style="color: #000000;">h</span><span style="color: #000000;">o</span><span style="color: #000000;">l</span><span style="color: #000000;">d</span> <span style="color: #6f6f6f;">T</span><span style="color: #000000;">h</span><span style="color: #000000;">r</span><span style="color: #000000;">e</span><span style="color: #000000;">e</span> <span style="color: #5c5c5c;">T</span><span style="color: #000000;">a</span><span style="color: #000000;">i</span><span style="color: #000000;">l</span><span style="color: #000000;">s</span><span style="color: #000000;">,</span> <span style="color: #000000;">L</span><span style="color: #000000;">L</span><span style="color: #000000;">C</span> <span style="color: #000000;">h</span><span style="color: #000000;">a</span><span style="color: #000000;">r</span><span style="color: #000000;">m</span><span style="color: #000000;">l</span><span style="color: #000000;">e</span><span style="color: #000000;">s</span><span style="color: #000000;">s</span> <span style="color: #000000;">a</span><span style="color: #000000;">g</span><span style="color: #000000;">a</span><span style="color: #000000;">i</span><span style="color: #000000;">n</span><span style="color: #000000;">s</span><span style="color: #000000;">t</span> <span style="color: #000000;">a</span><span style="color: #000000;">n</span><span style="color: #000000;">y</span> <span style="color: #000000;">c</span><span style="color: #000000;">l</span><span style="color: #000000;">a</span><span style="color: #000000;">i</span><span style="color: #000000;">m</span><span style="color: #000000;">s</span> <span style="color: #000000;">b</span><span style="color: #000000;">y</span> <span style="color: #000000;">p</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span><span style="color: #000000;">s</span><span style="color: #000000;">o</span><span style="color: #000000;">n</span><span style="color: #000000;">s</span> <span style="color: #000000;">o</span><span style="color: #000000;">r</span> <span style="color: #000000;">e</span><span style="color: #000000;">n</span><span style="color: #000000;">t</span><span style="color: #ebebeb;">i</span><span style="color: #000000;">t</span><span style="color: #efefef;">i</span><span style="color: #000000;">e</span><span style="color: #000000;">s</span> <span style="color: #000000;">o</span><span style="color: #000000;">t</span><span style="color: #000000;">h</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span> <span style="color: #000000;">t</span><span style="color: #000000;">h</span><span style="color: #000000;">a</span><span style="color: #000000;">n</span> <span style="color: #000000;">o</span><span style="color: #000000;">w</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span> <span style="color: #000000;">r</span><span style="color: #000000;">e</span><span style="color: #000000;">l</span><span style="color: #000000;">a</span><span style="color: #000000;">t</span><span style="color: #000000;">e</span><span style="color: #000000;">d</span> <span style="color: #000000;">t</span><span style="color: #000000;">o</span> <span style="color: #000000;">o</span><span style="color: #000000;">w</span><span style="color: #000000;">n</span><span style="color: #000000;">e</span><span style="color: #000000;">r</span><span style="color: #a0a0a0;">’</span><span style="color: #000000;">s</span> <span style="color: #000000;">d</span><span style="color: #000000;">o</span><span style="color: #000000;">g</span><span style="color: #000000;">(</span><span style="color: #000000;">s</span><span style="color: #000000;">)</span> <span style="color: #000000;">p</span><span style="color: #000000;">a</span><span style="color: #000000;">r</span><span style="color: #000000;">t</span><span style="color: #f3f3f3;">i</span><span style="color: #000000;">c</span><span style="color: #f8f8f8;">i</span><span style="color: #000000;">p</span><span style="color: #000000;">a</span><span style="color: #000000;">t</span><span style="color: #f9f9f9;">i</span><span style="color: #000000;">o</span><span style="color: #000000;">n</span><span style="color: #000000;">.</span></p>
            <p><span style="font-weight: bold; color: #000000;">O</span><span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">r</span> <span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">d</span> <span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">g</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">m</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">d</span> <span style="font-weight: bold; color: #000000;">u</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">d</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">d</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">n</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">g</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">m</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">o</span> <span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">d</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">m</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">f</span><span style="font-weight: bold; color: #000000;">y</span> <span style="font-weight: bold; color: #000000;">T</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">T</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">,</span> <span style="font-weight: bold; color: #000000;">L</span><span style="font-weight: bold; color: #000000;">L</span><span style="font-weight: bold; color: #000000;">C</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">d</span> <span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">r</span> <span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">d</span> <span style="font-weight: bold; color: #000000;">p</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">d</span> <span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">a</span> <span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">v</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">r</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">d</span> <span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">f</span> <span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">b</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">y</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">d</span> <span style="font-weight: bold; color: #000000;">a</span> <span style="font-weight: bold; color: #000000;">p</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">m</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">o</span> <span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">u</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">r</span> <span style="font-weight: bold; color: #000000;">m</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">k</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">a</span> <span style="font-weight: bold; color: #000000;">c</span><span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">m</span><span style="font-weight: bold; color: #000000;">.</span> <span style="font-weight: bold; color: #000000;">O</span><span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">r</span> <span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">g</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">m</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">a</span> <span style="font-weight: bold; color: #000000;">c</span><span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">c</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">b</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">n</span> <span style="font-weight: bold; color: #000000;">T</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">T</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">,</span> <span style="font-weight: bold; color: #000000;">L</span><span style="font-weight: bold; color: #000000;">L</span><span style="font-weight: bold; color: #000000;">C</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">d</span> <span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">,</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">d</span> <span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">r</span> <span style="font-weight: bold; color: #000000;">c</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">t</span> <span style="font-weight: bold; color: #000000;">p</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">c</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">p</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">n</span> <span style="font-weight: bold; color: #000000;">T</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">T</span><span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">,</span> <span style="font-weight: bold; color: #000000;">L</span><span style="font-weight: bold; color: #000000;">L</span><span style="font-weight: bold; color: #000000;">C</span> <span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">v</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">c</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">u</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">r</span> <span style="font-weight: bold; color: #000000;">s</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">g</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">a</span><span style="font-weight: bold; color: #000000;">g</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">m</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">t</span><span style="font-weight: bold; color: #000000;">,</span> <span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">h</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">c</span><span style="font-weight: bold; color: #000000;">h</span> <span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">r</span> <span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">s</span> <span style="font-weight: bold; color: #000000;">d</span><span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">n</span><span style="font-weight: bold; color: #000000;">g</span> <span style="font-weight: bold; color: #000000;">o</span><span style="font-weight: bold; color: #000000;">f</span> <span style="font-weight: bold; color: #000000;">f</span><span style="font-weight: bold; color: #000000;">r</span><span style="font-weight: bold; color: #000000;">e</span><span style="font-weight: bold; color: #000000;">e</span> <span style="font-weight: bold; color: #000000;">w</span><span style="font-weight: bold; color: #000000;">i</span><span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">l</span><span style="font-weight: bold; color: #000000;">.</span></p>
          </div>
        </div>
      </li>
      <li class="form-line form-line-column form-col-1 jf-required" data-type="control_textbox" id="id_51">
        <label class="form-label form-label-top" id="label_51" for="input_51">
          Signature of Owner
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_51" class="form-input-wide jf-required">
          <input type="text" id="input_51" name="q51_signatureOf" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" aria-labelledby="label_51" required="" />
        </div>
      </li>
      <li class="form-line form-line-column form-col-2" data-type="control_datetime" id="id_19">
        <label class="form-label form-label-top" id="label_19" for="lite_mode_19"> Date </label>
        <div id="cid_19" class="form-input-wide">
          <div data-wrapper-react="true">
            <div style="display:none">
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[disallowPast, limitDate]" id="month_19" name="q19_date19[month]" size="2" data-maxlength="2" maxLength="2" value="06" autoComplete="off" aria-labelledby="label_19 sublabel_19_month" />
                <span class="date-separate" aria-hidden="true">
                   /
                </span>
                <label class="form-sub-label" for="month_19" id="sublabel_19_month" style="min-height:13px" aria-hidden="false"> Month </label>
              </span>
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="currentDate form-textbox validate[disallowPast, limitDate]" id="day_19" name="q19_date19[day]" size="2" data-maxlength="2" maxLength="2" value="05" autoComplete="off" aria-labelledby="label_19 sublabel_19_day" />
                <span class="date-separate" aria-hidden="true">
                   /
                </span>
                <label class="form-sub-label" for="day_19" id="sublabel_19_day" style="min-height:13px" aria-hidden="false"> Day </label>
              </span>
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[disallowPast, limitDate]" id="year_19" name="q19_date19[year]" size="4" data-maxlength="4" data-age="" maxLength="4" value="2020" autoComplete="off" aria-labelledby="label_19 sublabel_19_year" />
                <label class="form-sub-label" for="year_19" id="sublabel_19_year" style="min-height:13px" aria-hidden="false"> Year </label>
              </span>
            </div>
            <span class="form-sub-label-container " style="vertical-align:top">
              <input type="text" class="form-textbox validate[disallowPast, limitDate, validateLiteDate]" id="lite_mode_19" size="12" data-maxlength="12" maxLength="12" data-age="" value="06/05/2020" data-format="mmddyyyy" data-seperator="/" placeholder="mm/dd/yyyy" autoComplete="off" aria-labelledby="label_19" />
              <img class=" newDefaultTheme-dateIcon icon-liteMode" alt="Pick a Date" id="input_19_pick" src="https://cdn.jotfor.ms/images/calendar.png" data-component="datetime" aria-hidden="true" data-allow-time="No" data-version="v1" />
              <label class="form-sub-label" for="lite_mode_19" id="sublabel_19_litemode" style="min-height:13px" aria-hidden="false">  </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_textbox" id="id_20">
        <label class="form-label form-label-top form-label-auto" id="label_20" for="input_20"> Name of Owner (please print) </label>
        <div id="cid_20" class="form-input-wide">
          <input type="text" id="input_20" name="q20_nameOf" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_20" />
        </div>
      </li>
      <li class="form-line" data-type="control_text" id="id_21">
        <div id="cid_21" class="form-input-wide">
          <div id="text_21" class="form-html" data-component="text">
            <p><span style="color: #000000;">3</span><span style="color: #000000;">9</span><span style="color: #000000;">3</span><span style="color: #000000;">1</span><span style="color: #000000;"> </span><span style="color: #000000;">L</span><span style="color: #000000;">i</span><span style="color: #000000;">l</span><span style="color: #000000;">l</span><span style="color: #000000;">i</span><span style="color: #000000;">a</span><span style="color: #000000;">n</span><span style="color: #000000;"> </span><span style="color: #000000;">G</span><span style="color: #000000;">u</span><span style="color: #000000;">e</span><span style="color: #000000;">s</span><span style="color: #000000;">t</span><span style="color: #000000;"> </span><span style="color: #000000;">C</span><span style="color: #eeeeee;">i</span><span style="color: #000000;">r</span><span style="color: #000000;">c</span><span style="color: #f4f4f4;">l</span><span style="color: #000000;">e</span><span style="color: #000000;">,</span><span style="color: #000000;"> </span><span style="color: #000000;">V</span><span style="color: #000000;">a</span><span style="color: #000000;">l</span><span style="color: #000000;">d</span><span style="color: #000000;">o</span><span style="color: #000000;">s</span><span style="color: #000000;">t</span><span style="color: #000000;">a</span><span style="color: #000000;">,</span><span style="color: #000000;"> </span><span style="color: #000000;">G</span><span style="color: #000000;">A</span><span style="color: #000000;"> </span><span style="color: #000000;">3</span><span style="color: #000000;">1</span><span style="color: #000000;">6</span><span style="color: #000000;">0</span><span style="color: #000000;">5</span><span style="color: #000000;"> </span><span style="color: #000000;">|</span><span style="color: #000000;"> </span><span style="color: #000000;">(</span><span style="color: #000000;">2</span><span style="color: #000000;">2</span><span style="color: #000000;">9</span><span style="color: #000000;">)</span><span style="color: #000000;">2</span><span style="color: #000000;">2</span><span style="color: #000000;">1</span><span style="color: #000000;">-</span><span style="color: #000000;">0</span><span style="color: #000000;">1</span><span style="color: #000000;">4</span><span style="color: #000000;">4</span><span style="color: #000000;"> </span><span style="color: #000000;">|</span><span style="color: #000000;"> </span><span style="color: #000000;">t</span><span style="color: #000000;">h</span><span style="color: #000000;">r</span><span style="color: #000000;">e</span><span style="color: #000000;">e</span><span style="color: #000000;">t</span><span style="color: #000000;">a</span><span style="color: #000000;">i</span><span style="color: #000000;">l</span><span style="color: #000000;">s</span><span style="color: #000000;">p</span><span style="color: #000000;">e</span><span style="color: #000000;">t</span><span style="color: #000000;">s</span><span style="color: #000000;">@</span><span style="color: #000000;">g</span><span style="color: #000000;">m</span><span style="color: #000000;">a</span><span style="color: #000000;">i</span><span style="color: #000000;">l</span><span style="color: #000000;">.</span><span style="color: #000000;">c</span><span style="color: #000000;">o</span><span style="color: #000000;">m</span></p>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_button" id="id_22">
        <div id="cid_22" class="form-input-wide">
          <div style="margin-left:156px" data-align="auto" class="form-buttons-wrapper form-buttons-auto   jsTest-button-wrapperField">
            <button id="input_preview_22" type="button" class="form-submit-preview" data-component="button">
              <img src="https://cdn.jotfor.ms/assets/img/theme-assets/5ca4930530899c64ff77cfa1/previewPDF-icon.svg" />
              <span id="span_preview_22" class="span_preview">
                Preview PDF
              </span>
            </button>
            <span>
               
            </span>
            <button id="input_22" type="submit" class="form-submit-button submit-button jf-form-buttons jsTest-submitField" data-component="button" data-content="">
              Submit
            </button>
          </div>
        </div>
      </li>
      <li style="display:none">
        Should be Empty:
        <input type="text" name="website" value="" />
      </li>
    </ul>
  </div>
  <script>
  JotForm.showJotFormPowered = "new_footer";
  </script>
  <script>
  JotForm.poweredByText = "Powered by JotForm";
  </script>
  <input type="hidden" id="simple_spc" name="simple_spc" value="201559273014046" />
  <script type="text/javascript">
  document.getElementById("si" + "mple" + "_spc").value = "201559273014046-201559273014046";
  </script>
  <div class="formFooter-heightMask">
  </div>

</form>
</body>

</html>`);
};

export default Agreement;
