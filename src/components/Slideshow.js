/* eslint-disable jsx-a11y/alt-text */
import React from 'react';

import Slider from 'react-slick';

import '../styles/slideshow.scss';
import '../../node_modules//slick-carousel/slick/slick.css';
import '../../node_modules/slick-carousel/slick/slick-theme.css';

const Slideshow = () => {
	const settings = {
		dots: false,
		infinite: true,
		speed: 500,
		slidesToShow: 3,
		slidesToScroll: 3,
		autoplay: true,
		autoplaySpeed: 3000,
		adaptiveHeight: false,
		centerMode: true,
		// centerPadding: '100px',
		rows: 1,
		slidesPerRow: 1
	};

	return (
		<div className="slides">
			<Slider {...settings}>
				<div>
					<img src="https://previews.dropbox.com/p/thumb/AA0E7_4wM782nXZ_B-SPPYWp6-PKQMxRNMnfL_xyeuurCAiKRro8g-bpgFuuiXPsyisNf9rpnXXsLHnf7voDA89oue9UFZEnFBwKklaWFVwvt4eJ6Dk8dsuoGzDIWsjsjD3OkhY-qTyMae4yJZDi0Q9_NfP3trOF0-jNVIYBOBAv-lXDB7L3hZWwznQ0Owu06VEMAnPSHPG1CBB28jxbztEYTZNfxRO8Sc53_7tIx6u-LjSDtH4lCoerj00h3a049WDGOQlMYVWYMDO0DTWYTBxYZVr_W0YDyR7ZrlB7iHHpDwMuKvPmQAZlrqyAZEu4aQfMVf4NBtNHK2UB3FIP3XIUDeabFTHgAqseL_iRZPM0ZA/p.jpeg?fv_content=true&size_mode=5" />
				</div>
				<div>
					<img src="https://previews.dropbox.com/p/thumb/AA1rcsSc9mvZ9lCdIfmDDzFyDzaI49ysiFs7p9wO2p-RY_OHwp1oF9mRR9Vt4Ac5f7CG4ss9BpH8OUFmB_DjEAQ9D0Zcpw41cIQXi6R9zyg1qYZ1QvcRSPUiQhkS31vRrWQUGWj5S-0hsjyGpGBLnrfh-y14glXm87Cm8K0bTBANyiacTci0_hF0vixSnTPrgG-AUkjewK1wt3Co43k_UYfwMTQGF3KwKNDmPlBDKu8q4AXBfXjvhwgYz75SWHWMMl5uXFkOxgSwhSOzICMcAoL3HuBRCOgCvjYBT4HDKCzWsJAsyA9z3nk5e9-k-UcOrhoivQaqZcfT_icZb3SOYBaa_G5sUnE25cSbH-QhbvqFTQ/p.jpeg?fv_content=true&size_mode=5" />
				</div>
				<div>
					<img
						src="https://previews.dropbox.com/p/thumb/AA34Dht2Q6VIv-0FPTRaCx190MixJ4-YYm5gwZ9XUBpg4-Q1cc5GcoTFMPuUisTFf6FCs_w56IjZD_p8US8yu1oDHRNS6RfwqJWiVyMIT1QPpxNZPTFw6OMJXNV2BP9QWF80Q1n3XX2_wgmYPx3bygaNlgM1_o_eqFZ-8-Rw6Ig5yqRy61EUN-hSyNeFu3aebZpW3JG3Ksiska1ozAsSNE8N6AukG3s2y5dZX-YRGp0qouUGhw3dMsn5m6KdjDavUB3ijLylOGpFmHfkbX5uFp2_TKw8K_DdH-2Eaknxm7jt49Nq-mtzpucMmuosP80YnC8ehMT40x2rpEPH9L_QHlt9iz0KJIjGXB0of1AktOGrOA/p.jpeg?fv_content=true&size_mode=5"
						alt=""
					/>
				</div>
				<div>
					<img
						src="https://previews.dropbox.com/p/thumb/AA0AUKO-fVbOA2gOdgIsDaCBz7GIhjXST6dv872EYJAmJjuQljNkreTcpwS2oZdL4NTWeQlZYEvczglwAWia4RUHp786nlfdxR9-kJGdHdjdHs9iloAtDjcBb8MeuHuUTf6NI5q2MB0J-7vS39V5l3anXK8eM60YX2fOZcZmXP5rcDWB9Fp3cP0xBCbr6MwhUUNmIydxR-u1BaWV3Hxs2iYnogvZ7riQQi0V99-DdBiorDEpWs4AaXu6ea7C5hln-7UxqNFOz7FE3zuoeEKWcC4oyM5I12sZ5biseP0urPHDtaCPi-YS8Vv3XNTdZzaXkrJZxy-ePgYLUP3W5WzpuebFPDz1IzQnxNo2b-X9z_koKQ/p.jpeg?size=2048x1536&size_mode=3"
						alt=""
					/>
				</div>
				<div>
					<img
						src="https://previews.dropbox.com/p/thumb/AA0I0NtwWaixgDHtb9I-01F-4OUs6HKOINvJBrf1RmUzE1Ho2MR82Od902lz4Tk3BzwKUrKrYvclVrvz1FHKS44ty4OOOgM9eZ31Xk2BXfK8kClorcmJe2VlrKaILIpSDBUigK_9lyPjbwOH3vkUs0EOP6iKYvKoSTHC1vuAARQ9geVeQoFK5SK6FD9x_jCPOq3YLsa0HxEbW-Q9U4L17KbScULzBowWaKFiL-MfH037n00hEnIk3XtXzz94I1bp3yx6QzJwN2i18E9cM3AckHMGK_zNfki_CJ2k-BSnCRE0M3Nt4a2fNdJZ6TvTg0cnOV6XzVaMWRCdt_tZdRSgeYvqWlzXUEGFsXVEtGIcrAEhlw/p.jpeg?fv_content=true&size_mode=5"
						alt=""
					/>
				</div>
				<div>
					<img
						src="https://previews.dropbox.com/p/thumb/AA1GzvZhT6B02xiPlPCdM9akgfcLEBRcs7ul_DoOUfB2D4o_w269rNSCzuIM5oxUkheCa7m3VsIy-GypeX-FHZ0PVTEO4WjHrauzjl2_OBqvwtlTfsLrV40y_O7sjSNNdeHZNiZ_3ECzNI2EZj80MGZMz5c3ibI1evdsATBfnDzflCXExeoMtxwcCCOFU0Wrwt2I4xpZBWdo4ty4cP49LoNo5-F5q3KMfoX3kDr5IjXDg9y1PjdvTO_hQF9GJbGoSM4iGN0gbUluYl23bnC1Pr_VPdB-KKY2DgyFhp-5QX_H8T2JaUERWHIQkwUu5gc_WyoGc5-R7nuayGFgfjtBt4HfCgN1mylr4SPSZLHJ6av2tg/p.jpeg?fv_content=true&size_mode=5"
						alt=""
					/>
				</div>
				<div>
					<img
						src="https://previews.dropbox.com/p/thumb/AA1hCLRs0bKfnRkGWukIaJ6Rh9aHDgA6IhnxkPvFUeNu5CPmb6A3bgWURXi-jLZEXlprtZ1sjnDtpB0jsxpFzrhfLtZBzK3XNCRgFhMtCQD91CgUFZ9C3SV-bJv547u893yz_TzuribXnU6OhQxT9X6U8u_vCT-ORkNCb0gmSWc5Lk7mKBWh-Ettyb0Qdd-5UzMcVKVezRmQainbidT4h__JDcEWlvHxWXCP1OYWKSlTKtrzOtuCZyJx5lOWbo_OAOZSeWGqWm2AK-HudhmBV5fc-MkbUXc2BnP8WVE6p0S1ePOnKT2d-n_SIwis3QEwGwCurYpdheOtVxeCGKkYy0hkfW9_j8_gTBI9NkKNIgb11Q/p.jpeg?fv_content=true&size_mode=5"
						alt=""
					/>
				</div>
				<div>
					<img
						src="https://previews.dropbox.com/p/thumb/AA1hhdm42r_bj3uEPFOdjSc2WqZQEVLG_MU5zl9uUZzbBmk1LdoCH3ywJoD2tQ9O8Pu4UH8_8YQVsORmEWK5V48VBY4KsMPNG3ptTZhwyLQ8RTK2v71PU5AN5NyfQ1iLUJ_FOghI1Qow80-U1unaIc1hHKyLyDckGqQsPmF_lTuxvO1dyTtJMwZLkEP_ltNnmJp9A_KHU310SjNX6eL0r4Gx8Al_dyaRnRtxiZuyMTkpUzzMyZLSq6khKNzhEn0xbOXz2TWuFKuLXb4nrzis-e_gxIiJLC6gHESdTcW2P00m1WOnAOSSEf1LJRjWgE8KdvOnRUQJIiuze6WE6IxnwtPUwe2i1hvvcD7r4QVqnMWCag/p.jpeg?size=2048x1536&size_mode=3"
						alt=""
					/>
				</div>
			</Slider>
		</div>
	);
};
export default Slideshow;
