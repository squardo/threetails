import React from 'react';

import Logo from '../images/TT-LOGO-PNG.png';
import '../styles/footer.scss';

const Footer = () => {
	return (
		<div>
			<div className="footer">
				<div className="logo">
					<img src={Logo} alt="" />
				</div>{' '}
				<div className="footer-row middle">
					<a href="terms_of_service">Important Files</a>
					<p className="non-click">©ThreeTailsPetsLLC</p>
				</div>
				<div className="footer-row">
					<a href="https://form.jotform.com/201559273014046">Contact Me:</a>
					<a href="email">threetailspets@gmail.com</a>
					<p>(229) 221-0144</p>
				</div>
			</div>
		</div>
	);
};

export default Footer;
