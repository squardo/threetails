import React from 'react';

const Body = () => {
	return (
		<div className="container">
			<div className="container has-text-centered">
				<h1 className="title m2">About Three Tails</h1>
				<p className="bodytext">
					Hey y'all! I'm Hannah, I am 23 years old. I am a full time veterinary
					technician and run a pet sitting business on the side. Ever since I
					was little, I've had a passion for animals. I started pet-sitting a
					sweet Golden Retriever back in 2013 as my first "job", and that was
					just the start of something much bigger! Three tails was coined in
					2018 when I had gotten so much word-of-mouth business that I devided
					to make it official and give my business a name. Fast forward to 2020
					and I am a licensed pet sitter who offers a wide variety of services
					to ensure your precious four legged friend is happy and healthy!
				</p>
			</div>
			<div className="bodyLower">
				<div className="body-left">
					<h1 className="title">Drop in visits</h1>
					<p>
						Have a particular pup that doesn't do well in new environments? Or
						cats that just need a little attention during the day? No problem!
						One of the services I offer are drop in visits, where I come to YOUR
						home and care for your fury companion. This way, they are in the
						comfort of their own home and don't have to stress in a new
						environment.
					</p>
				</div>
				<div className="body-right">
					<h1 className="title">In Home Boarding</h1>
					<p>
						I also offer pet sitting in my home to those that are active,
						friendly and need a little more one on one attention. Some pets
						don't do well kenneled for long periods of time, and that's okay!
						That is why Three Tails offers in home boarding. It's boarding minus
						the kennels and in the comfort of a home. It's a win-win for
						everyone!
					</p>
				</div>
			</div>
		</div>
	);
};

export default Body;
