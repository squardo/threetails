import React from 'react';

import Nav from '../components/Nav';
import Hero from '../components/Hero';
import Body from '../components/Body';
import Slideshow from '../components/Slideshow';
import Footer from '../components/Footer';

const Home = () => {
	return (
		<div>
			<Nav />
			<Hero />
			<Body />
			<div className="container">
				<Slideshow />
			</div>
			<Footer />
		</div>
	);
};

export default Home;
