import React from 'react';
import Nav from '../components/Nav';
import Agreement from '../components/Agreement';

const TermsOfService = () => {
	return (
		<div>
			<Nav />
			<Agreement />
		</div>
	);
};

export default TermsOfService;
