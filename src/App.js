import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from './views/Home';
import Error from './views/Error';
// import TermsOfService from './views/TermsOfService';

function App() {
	return (
		<div>
			<Router>
				<Switch>
					<Route path="/" component={Home} exact />
					<Route
						path="/RainbowProto"
						exact
						render={() => {
							window.location.href = './forms/RainbowProto/Rainbow.html';
						}}
					/>
					<Route component={Error} />
				</Switch>
			</Router>
		</div>
	);
}

export default App;
